<?php

class View
{
	protected $_head, $_body, $_editForm, $_siteTitle = SITE_TITLE, $_outputBuffer, $_layout = 'default', $_searchResults = null;

	public function __construct()
	{

	}

	public function render($viewName)
	{
		$viewAry = explode('/', $viewName);
		$viewString = implode(DS, $viewAry);

		if(file_exists(ROOT.DS.'app'.DS.'views'.DS.$viewString.'.php')){
			include (ROOT.DS.'app'.DS.'views'.DS.$viewString.'.php');
			include (ROOT.DS.'app'.DS.'views'.DS.'layouts'.DS.$this->_layout.'.php');
		} else{
			die('The view \"'.$viewName.'\" does not exist.');
		}
	}

	public function content($type)
	{
		if($type == 'head'){
			return $this->_head;
		} elseif ($type == 'body'){
			return $this->_body;
		} elseif ($type == 'editForm'){
			return $this->_editForm;
		}
		return false;
	}

	public function start($type)
	{
		$this->_outputBuffer = $type;
		ob_start();
	}

	public function end()
	{
		if($this->_outputBuffer == 'head'){
			$this->_head = ob_get_clean();
		} elseif($this->_outputBuffer == 'body'){
			$this->_body = ob_get_clean();
		} elseif($this->_outputBuffer == 'editForm'){
			$this->_body = ob_get_clean();
		} else {
			die('You must first run the start method.');
		}
	}

	public function siteTitle()
	{
		return $this->_siteTitle;
	}

	public function setSiteTitle($title)
	{
		$this->_siteTitle = $title;
	}

	public function setLayout($path)
	{
		$this->_layout = $path;
	}

	public function setSearchResults($results)
	{
		$this->_searchResults = $results;
	}

	public function searchResults()
	{
		return $this->_searchResults;
	}

}