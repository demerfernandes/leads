<?php

class Model
{
	protected $_db, $_table, $_modelName, $_softDelete = false, $_columnNames = [];
	public $id;

	public function __construct($table)
	{
		$this->_db = DB::getInstance();
		$this->_table = $table;
		$this->_setTableColumns();
		$this->_modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', $this->_table)));
	}

	protected function _setTableColumns()
	{
		$columns = $this->get_columns();
		foreach ($columns as $c) {
			$columnName = $c->Field;
			$this->_columnNames[] = $c->Field;
			$this->{$columnName} = null;
		}
	}

	public function get_columns()
	{
		return $this->_db->getColumns($this->_table);
	}

	public function columnNames()
	{
		return $this->_columnNames;
	}

	protected function _softDeleteParams($params)
	{
		if ($this->_softDelete) {
			if (array_key_exists('conditions', $params)) {
				if (is_array($params['conditions'])) {
					$params['conditions'][] = "deleted != 1";
				} else {
					$params['conditions'] .= " AND deleted != 1";
				}
			} else {
				$params['conditions'] = "deleted != 1";
			}
		}
		return $params;
	}

	public function find($params = [])
	{
		$params = $this->_softDeleteParams($params);
		$results = [];
		$resultQuery = $this->_db->find($this->_table, $params);
		if ($resultQuery && is_array($resultQuery)) {
			foreach ($resultQuery as $result) {
				$obj = new $this->_modelName($this->_table);
				$obj->parseData($result);
				$results[] = $obj;
			}
			return $results;
		}
		return false;
	}

	public function findFirst($params = [])
	{
		$params = $this->_softDeleteParams($params);
		$resultQuery = $this->_db->findFirst($this->_table, $params);
		$obj = new $this->_modelName($this->_table);
		if ($resultQuery) {
			$obj->parseData($resultQuery);
		}
		return $obj;
	}

	public function findByID($id)
	{
		return $this->findFirst(['conditions' => "id = ?", 'bind' => [$id]]);
	}

	public function save()
	{
		$fields = [];
		foreach ($this->_columnNames as $column) {
			if (!is_null($this->$column)) $fields[$column] =  $this->$column;
			// $fields[$column] = $this->$column;
		}
		// determine whether to update or insert
		if (property_exists($this, 'id') && $this->id != '') {
			return $this->update($this->id, $fields);
		} else {
			return $this->insert($fields);
		}
	}

	public function insert($fields)
	{
		if (empty($fields)) return false;
		return $this->_db->insert($this->_table, $fields);
	}

	public function update($id, $fields)
	{
		if (empty($fields) || $id == '') return false;
		return $this->_db->update($this->_table, $id, $fields);
	}

	public function delete($id = '')
	{
		if ($id == '' && $this->id == '') return false;
		$id = ($id == '') ? $this->id : $id;
		if ($this->_softDelete) {
			return $this->update($id, ['deleted' => 1]);
		} else {
			return $this->_db->delete($this->_table, $id);
		}
	}

	public function query($sql, $bind = [])
	{
		return $this->_db->query($sql, $bind);
	}

	public function data()
	{
		$data = new stdClass();
		foreach ($this->_columnNames as $column) {
			$data->column = $this->column;
		}
	}

	public function assign($params)
	{
		if (!empty($params)) {
			foreach ($params as $k => $v) {
				if (in_array($k, $this->_columnNames)) {
					$this->$k = cleanup($v);
				}
			}
			return true;
		}
		return false;
	}

	protected function parseData($data)
	{
		foreach ($data as $k => $v) {
			$this->$k = $v;
		}
	}

	public function setTable($table)
	{
		return $this->_table = $table;
	}

	public function lastInsertId()
	{
		return $this->_db->lastID();
	}

	public function count()
	{
		return $this->_db->count();
	}

	public function getCurrentDate()
	{
		date_default_timezone_set('America/Sao_Paulo');
		$date = date("Y-m-d H:i:s");
		return $date;
	}
}
