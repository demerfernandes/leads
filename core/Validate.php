<?php
class Validate
{
	private $_passed=false, $_errors=[], $_db=null; 
	
	public function __construct()
	{
		$this->_db = DB::getInstance();
	}

	public function check($source, $items=[])
	{
		$_errors = [];

		foreach($items as $item => $rules){
			$item = Input::sanitize($item);
			$display = $rules['display'];
			foreach($rules as $rule => $rule_value){
				$value = Input::sanitize(trim($source[$item]));

				if($rule === 'required' && empty($value)){
					$this->addError(["{$display} é obrigatório!", $item]);
				} else if(!empty($value)){
					switch ($rule) {
						case 'contains':
							if (!in_array($value, $rule_value)){
								$this->addError(["{$display} não confere.", $item]);	
							}
							break;
						case 'min':
							if(strlen($value) < $rule_value){
								$this->addError(["{$display} deve ser no mínimo {$rule_value} caracteres.", $item]);
							}
							break;

						case 'max':
							if(strlen($value) > $rule_value){
								$this->addError(["{$display} dever ser no máximo {$rule_value} caracteres.", $item]);
							}
							break;

						case 'matches':
							if($value != $source[$rule_value]){
								$matchDisplay = $items[$rule_value]['display'];
								$this->addError(["{$matchDisplay} e {$display} devem ser idênticos.", $item]);
							}
							break;

						case 'unique':
							$check = $this->_db->query("SELECT {$item} FROM {$rule_value} WHERE {$item}=?", [$value]);
							if($check->count()){
								$this->addError(["{$display} já existe. Favor escolher outro {$display}.", $item]);
							}
							break;

						case 'exists':
							$check = $this->_db->query("SELECT * FROM {$rule_value['table']} WHERE {$rule_value['columns']}=?", [$value]);
							if(!$check->count()){
								$this->addError(["{$display} não existe.", $item]);
							}
							break;

						case 'unique_update':
							$t = explode(',', $rule_value);
							$table = $t[0];
							$id = $t[1];
							$query = $this->_db->query("SELECT * FROM {$table} WHERE id != ? AND {$item} = ?",[$id, $value]);
							if($query->count()){
								$this->addError(["{$display} já existe. Favor escolher outro {$display}", $item]);
							}
							break;

						case 'is_numeric':
							if(!is_numeric($value)){
								$this->addError(["{$display} deve ser um número. Favor usar valores numéricos", $item]);
							}
							break;

						case 'valid_email':
							if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
								$this->addError(["{$display} é obrigatório ser um e-mail válido.", $item]);
							}
							break;
					}
				}
			}
		}

		if(empty($this->_errors)){
			$this->_passed = true;
		}
		return $this;
	}

	public function validatePost($post){
		$this->_errors = [];
		
		$valid = [];
		if (is_array($post) || $post != ''){
			foreach ($post as $key => $value) {
				$key = Input::sanitize($key);
				$value = Input::sanitize($value);
				if ($value != '') {
					$valid[$key] = $value;
				}
			}
		}

		if (empty($valid)){
			$this->addError("Preencha pelo menos um parâmetro.");
		} 
		if(empty($this->_errors)){
			$this->_passed = true;
		}
		return $valid;
	}

	public function addError($error){
		$this->_errors[] = $error;
		if(empty($this->_errors)){
			$this->_passed = true;
		} else{
			$this->_passed = false;
		}
	}

	public function errors()
	{
		return $this->_errors;
	}

	public function passed()
	{
		return $this->_passed;
	}

	public function displayErrors()
	{
		$html = '<ul>';
		foreach ($this->_errors as $e){
			if(is_array($e)){
				$html .= '<li class="text-danger">'.$e[0].'</li>';
				$html .= '<script>jQuery("document").ready(function(){jQuery("#'.$e[1].'").addClass("is-invalid");});</script>';
				$html .= '<script>jQuery("document").ready(function(){jQuery("#'.$e[1].'").after("<div class=\"invalid-feedback\">Preencha este campo corretamente!</div>");});</script>';
			} else {
				$html .= '<li class="text-danger">'.$e.'</li>';
			}
		}
		$html .= '</ul>';
		return $html;
	}
}