<?php

function dnd($var){
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
	die();
}

function cleanup($dirty){
	return htmlentities($dirty, ENT_QUOTES, 'UTF-8');
}

function currentUser(){
	return Users::currentLoggedInUser();
 }

function posted_values($post){
	$clean_ary = [];
	foreach ($post as $k => $v) {
		$clean_ary[$k] = cleanup($v);
	}
	return $clean_ary;
}

function currentPage(){
	$currentPage = $_SERVER['REQUEST_URI'];

	if($currentPage == PROOT || $currentPage == PROOT.'dashboard/index'){
		$currentPage = PROOT.'dashboard';
	}
	return $currentPage;
}

function searchAccess($database='Regin',$params='results_params'){
	$acl_file = file_get_contents(ROOT.DS.'app'.DS.'search_acl.json');
	$acl = json_decode($acl_file, true);
	$current_user_acls = ["Basic"];
	$acl_params = [];
	$result_params = [];
	foreach (currentUser()->teams() as $a) {
		$current_user_acls[] = $a;
	}

	//return params
	foreach ($current_user_acls as $access){
		if(array_key_exists($access, $acl)){
			foreach ($acl[$access][$database][$params] as $p) {
				if(!in_array($p, $acl_params)){
					$acl_params[$acl['Dictionary'.$database][$p]] = $p;
				}
			}
		}
	}
	return $acl_params;
}