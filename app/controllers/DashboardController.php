<?php
class DashboardController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->view->setLayout('default');
		$this->load_model('Leads');
	}

	public function indexAction()
	{
		$this->view->render('dashboard/index');
	}

	public function dashboardAction()
	{
		$this->view->render('dashboard/dashboard');
	}

	public function processDisplayAction()
	{
		$type = (isset($_POST['type']) && Input::get('type') != '') ? Input::get('type') : 'card';
		$period = (isset($_POST['period'])) ? Input::get('period') : '';
		$last_contact_status = (isset($_POST['last_contact_status'])) ? Input::get('last_contact_status') : '';
		$status = (isset($_POST['status'])) ? Input::get('status') : '';

		$table = 'leads_' . currentUser()->company_id;
		$user_id = currentUser()->id;
		$current_user_acls = [];

		foreach (currentUser()->acls() as $a) {
			$current_user_acls[] = $a;
		}
		// dnd($current_user_acls);

		$params = [
			'conditions' => [
				'deleted = 0'
			],
			'bind' => [],
			'limit' => 500,
			'order' => 'created_at DESC'
		];

		// Mostrar leads de todos se usuário for Admin ou Master
		if (in_array('Master', $current_user_acls) || in_array('Admin', $current_user_acls)) {
			$by_user_id = (isset($_POST['by_user_id'])) ? Input::get('by_user_id') : '';
			if ($by_user_id != '') {
				array_push($params['conditions'], 'user_id = ?');
				array_push($params['bind'], $by_user_id);
			}
		} elseif (in_array('User', $current_user_acls)) {
			array_push($params['conditions'], 'user_id = ?');
			array_push($params['bind'], $user_id);
		}

		switch ($last_contact_status) {
			case 'semcontato':
				array_push($params['conditions'], 'last_contact_status = ?');
				array_push($params['bind'], 'semcontato');
				break;

			case 'desconhecido':
				array_push($params['conditions'], 'last_contact_status = ?');
				array_push($params['bind'], 'desconhecido');
				break;

			case 'indisponivel':
				array_push($params['conditions'], 'last_contact_status = ?');
				array_push($params['bind'], 'indisponivel');
				break;

			case 'atendido':
				array_push($params['conditions'], 'last_contact_status = ?');
				array_push($params['bind'], 'atendido');
				break;

			case 'retorno':
				array_push($params['conditions'], 'last_contact_status = ?');
				array_push($params['bind'], 'retorno');
				break;

			default:
				break;
		}

		switch ($status) {
			case 'prospeccao':
				array_push($params['conditions'], 'status = ?');
				array_push($params['bind'], 'prospeccao');
				break;

			case 'qualificado':
				array_push($params['conditions'], 'status = ?');
				array_push($params['bind'], 'qualificado');
				break;

			case 'proposta':
				array_push($params['conditions'], 'status = ?');
				array_push($params['bind'], 'proposta');
				break;

			case 'negociacao':
				array_push($params['conditions'], 'status = ?');
				array_push($params['bind'], 'negociacao');
				break;

			case 'conversao':
				array_push($params['conditions'], 'status = ?');
				array_push($params['bind'], 'conversao');
				break;

			case 'em_andamento':
				array_push($params['conditions'], 'status != ?');
				array_push($params['bind'], 'conversao');
				break;

			case 'sim':
				array_push($params['conditions'], 'status  = ?');
				array_push($params['conditions'], 'conversion = ?');
				array_push($params['bind'], 'conversao');
				array_push($params['bind'], 'sim');
				break;

			case 'nao':
				array_push($params['conditions'], 'status = ?');
				array_push($params['conditions'], 'conversion = ?');
				array_push($params['bind'], 'conversao');
				array_push($params['bind'], 'nao');
				break;

			default:
				break;
		}

		// filtrando por periodo
		array_push($params['conditions'], 'DATEDIFF(NOW(), `created_at`) < ?');

		switch ($period) {

			case '7 dias':
				array_push($params['bind'], '7');
				break;

			case '15 dias':
				array_push($params['bind'], '15');
				break;

			case '30 dias':
				array_push($params['bind'], '30');
				break;

			default:
				array_push($params['bind'], '30');
				break;
		}

		// dnd($params);


		$leads = new Leads();
		$results = $leads->findCompanyLeads($table, $params);

		$count = $leads->count();

		if ($type == 'chart') return	$this->jsonResponse($results);
		if ($type == 'card') return $this->jsonResponse($count);
	}

	public function displayAction()
	{
		$this->view->render('dashboard/display');
	}

	public function autoAction()
	{
		$this->view->render('dashboard/auto');
	}
	public function autoCompleteCompanyAction()
	{
		if (!empty($_POST["keyword"])) {

			$this->load_model('Companies');
			$companies = new Companies();
			$suggestion = $companies->findByName(Input::get('keyword'));
			// $suggestion = $companies->findAll();

			if (!empty($suggestion)) {
				$this->jsonResponse($suggestion);
			}
		}
	}

	public function process($periodo, $lastContact, $status)
	{
		# code...
	}
}
