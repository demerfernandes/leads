<?php

class CompaniesController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->load_model('Companies');
		$this->view->setLayout('default');
	}

	public function indexAction()
	{
		$companies = $this->CompaniesModel->findAll();
		if ($companies) {
			$this->view->setSearchResults($companies);
		}
		$this->view->render('companies/index');
	}

	public function registerAction()
	{
		$validation = new Validate();

		$posted_values = ['razao_social' => '', 'cnpj' => '', 'company_acl' => '', 'nome_fantasia' => ''];
		if ($_POST) {
			$posted_values = posted_values($_POST);
			$validation->check($_POST, [
				'razao_social' => [
					'display' => 'Razão Social',
					'required' => true,
					'unique' => 'companies'
				],
				'cnpj' => [
					'display' => 'CNPJ',
					'required' => true,
					'unique' => 'companies',
					'is_numeric' => true
				]
			]);
		}


		if ($validation->passed()) {
			$company = new Companies();
			$company->registerNewCompany($_POST);
			Router::redirect('companies/register');
		}
		$this->view->post = $posted_values;
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->render('companies/register');
	}

	public function updateAction()
	{
		/*if($_POST){

			$params = [];
			alterar para nunca atualizar o cnpj
			if (array_key_exists('cnpj',$_POST))
			{
				array_push($params['cnpj'],Input::get('cnpj'));
			}
		
		}*/
		$company = new Companies();
		$results = $company->findById(intval(Input::get('id')));
		$params = [];
		$params = posted_values($_POST);
		if ($results->cnpj) {
			$params['cnpj'] = $results->cnpj;
		}
		$company->updateCompany($params); //alterar para passar o array $params
	}
	public function searchAction()
	{
		if ($_POST) {
			$post_params = trim($_POST["params"]);
			$company = $this->CompaniesModel->findById($post_params);
			$this->jsonResponse($company);
		}
	}

	public function processAction()
	{
		$post_params = currentUser()->company_id;

		$company = $this->CompaniesModel->findAll($post_params);
		$data = array(
			"data" => $company
		);
		$this->jsonResponse($data);
	}

	public function resultsAction()
	{
		if ($_POST) {
			$params = [];
			$company = new Companies();
			$table = 'companies_' . currentUser()->company_id;
			$params = [
				't1' => 'empresas',
				't2' => $table,
				'on_t2' => 't2.consulta_id',
				'conditions' => [
					't2.consulta_id = t1.id',
					't2.deleted = t1.deleted',
					't2.deleted = 0'
				],
				'limit' => 300,
				'order' => 't2.last_update'
			];
			$companies = $this->CompaniesModel->findAll($params);
			$this->jsonResponse($companies);
		}
	}

	public function modalAction()
	{
		$this->view->render('companies/modal');
	}
}
