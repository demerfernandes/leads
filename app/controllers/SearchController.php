<?php

class SearchController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->load_model('Empresas');
	}

	public function indexAction()
	{
		$this->view->render('search/index');
	}

	public function resultsAction()
	{
		$this->view->render('search/results');
	}

	public function processAction()
	{
		$validation = new Validate();

		if ($_POST) {

			$valid_params = $validation->validatePost($_POST);
			$table = 'leads_' . currentUser()->company_id;

			$validation->check($_POST, [
				'municipio' => [
					'display' => 'Município',
				],
				'cnpj' => [
					'display' => 'CNPJ',
					'is_numeric' => true
				]
			]);


			if (!empty($valid_params) && $validation->passed()) {
				$data['results'] = $this->EmpresasModel->findByParams($valid_params, $table);
				$this->jsonResponse($data['results']);
			} else if (!$validation->passed()) {
				$data['errors'] = $validation->displayErrors();
				$this->jsonResponse($data);
			}
		}
	}
}
