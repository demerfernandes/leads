<?php


class RegisterController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->load_model('Users');
		$this->view->setLayout('default');
	}

	public function indexAction()
	{
		if(currentUser()) return Router::redirect('dashboard/index');
		Router::redirect('register/login');
	}

	public function loginAction()
	{
		$validation = new Validate();

		if ($_POST) {
			//form validation
			$validation->check($_POST, [
				'username' => [
					'display' => 'Nome de Usuário',
					'required' => true
				],
				'password' => [
					'display' => 'Senha',
					'required' => true,
					'min' => 6
				]
			]);

			if ($validation->passed()) {
				$user = $this->UsersModel->findByUsername(Input::get('username'));
				if ($user && password_verify(Input::get('password'), $user->password)) {
					$remember = (isset($_POST['remember_me']) && Input::get('remember_me')) ? true : false;
					$user->login($remember);
					Router::redirect('');
				} else {
					$validation->addError("Existe um erro no Nome de Usuário ou Senha");
				}
			}
		}
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->render('register/login');
	}

	public function logoutAction()
	{
		if (currentUser()) {
			currentUser()->logout();
		}
		Router::redirect('register/login');
	}

	public function registerAction()
	{
		$validation = new Validate();
		$posted_values = ['fname' => '', 'company_name' => '', 'phone' => '',  'lname' => '', 'username' => '', 'email' => '',  'company_id' => '', 'password' => '', 'confirm' => '', 'position' => '', 'acl' => '', 'team' => '', 'address' => '', 'b_date' => ''];
		if ($_POST) {
			$posted_values = posted_values($_POST);

			$validation->check($_POST, [
				'fname' => [
					'display' => 'Nome',
					'required' => true
				],
				'lname' => [
					'display' => 'Sobrenome',
					'required' => true
				],
				'username' => [
					'display' => 'Nome de Usuário',
					'required' => true,
					'unique' => 'users',
					'min' => 6,
					'max' => 150
				],
				'company_id' => [
					'display' => 'Nome da Empresa',
					'required' => true
				],
				'position' => [
					'display' => 'Cargo',
					'required' => true
				],
				'team' => [
					'display' => 'Equipe',
					'required' => true,
					'contains' => ["Contabilidade", "Comercial", "Juridico"]
				],
				'acl' => [
					'display' => 'Tipo de Usuário',
					'required' => true,
					'contains' => ["Master", "User", "Admin"]
				],
				'phone' => [
					'display' => 'Telefone',
					'required' => true,
				],
				'email' => [
					'display' => 'E-mail',
					'required' => true,
					'unique' => 'users',
					'valid_email' => true,
					'max' => 150
				],
				'password' => [
					'display' => 'Senha',
					'required' => true,
					'min' => 6
				],
				'confirm' => [
					'display' => 'Confirm a Senha',
					'required' => true,
					'matches' => 'password'
				]
			]);

			$ary[] = $_POST['acl'];
			$_POST['acl'] = json_encode($ary);
			unset($ary);
			$ary[] = $_POST['team'];
			$_POST['team'] = json_encode($ary);

			if ($validation->passed()) {
				unset($_POST['company_name']);
				$newUser = new Users();
				$newUser->registerNewUser($_POST);
				Router::redirect('');
			}
		}
		$this->view->post = $posted_values;
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->render('register/register');
	}

	public function profileAction()
	{
		if (currentUser()) {
			$posted_values = ['fname' => currentUser()->fname, 'lname' => currentUser()->lname, 'username' => currentUser()->username, 'email' => currentUser()->email, 'position' => currentUser()->position, 'phone' => currentUser()->phone,'image' => currentUser()->image, 'b_date' => currentUser()->b_date, 'acl' =>  str_replace('"', '',currentUser()->acl), 'team' =>  str_replace('"', '',currentUser()->team)];
		}
		$validation = new Validate();
		if ($_POST) {
			$_POST['username'] = currentUser()->username;
			$_POST['email'] = currentUser()->email;
			$posted_values = posted_values($_POST);

			$validation->check($_POST, [
				'fname' => [
					'display' => 'Nome',
					'required' => true
				],
				'lname' => [
					'display' => 'Sobrenome',
					'required' => true
				]
			]);


			if ($validation->passed()) {
				$currentUser = Users::currentLoggedInUser();
				$currentUser->updateUser($_POST);
				Router::redirect('register/profile');
			}
		}
		$this->view->post = $posted_values;
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->render('register/profile');
	}

	public function passwordAction()
	{
		if (currentUser()) {
			$validation = new Validate();
			// $posted_values = ['password' => '', 'confirm' => ''];
			if ($_POST) {
				// $posted_values = posted_values($_POST);
				$validation->check($_POST, [
					'password' => [
						'display' => 'Senha',
						'required' => true,
						'min' => 6
					],
					'confirm' => [
						'display' => 'Confirme a Senha',
						'required' => true,
						'matches' => 'password'
					]
				]);

				if ($validation->passed()) {
					$currentUser = Users::currentLoggedInUser();
					$currentUser->updateUser($_POST);
					Router::redirect('register/logout');
				}
			}
		}
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->render('register/password');
	}
	public function profileImageAction(){
	
		function compressImage($source, $destination, $quality) { 
			// Get image info 
			$imgInfo = getimagesize($source); 
			$mime = $imgInfo['mime']; 
			 
			// Create a new image from file 
			switch($mime){ 
				case 'image/jpeg': 
					$image = imagecreatefromjpeg($source); 
					break; 
				case 'image/png': 
					$image = imagecreatefrompng($source); 
					break; 
				case 'image/gif': 
					$image = imagecreatefromgif($source); 
					break; 
				default: 
					$image = imagecreatefromjpeg($source); 
			} 
			 
			// Save image 
			imagejpeg($image, $destination, $quality); 
			// Return compressed image 
			return $destination; 
		} 
		 
		$validation = new Validate();
		// File upload path 
		$uploadPath = "public/imgs/profile/"; 
		 
		// If file upload form is submitted 
		$status = $displayErrors = ''; 
		if(isset($_POST)){ 
			
			$status = 'error'; 
			if(!empty($_FILES["profile-img"]["name"])) { 
				// File info 

				$name = $_FILES[ 'profile-img' ][ 'name' ];
				$extension = pathinfo ( $name, PATHINFO_EXTENSION );
				$extension = strtolower ( $extension );
				$newName =  uniqid ( time () ) . '.' . $extension;
				$imageUploadPath = $uploadPath . $newName; 
				$fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION);

				// Allow certain file formats 
				$allowTypes = array('jpg','png','jpeg','gif'); 
				if(in_array($fileType, $allowTypes)){ 
					// Image temp source 
					$imageTemp = $_FILES["profile-img"]["tmp_name"]; 
					
					// Compress size and upload image 
					$compressedImage = compressImage($imageTemp, $imageUploadPath, 10); 
					if($compressedImage){ 
						
						$currentUser = Users::currentLoggedInUser();
						$postImage = ['image' => $compressedImage];
						$currentUser->updateUser($postImage);
						$status = 'success'; 
						$validation->addError("Imagem alterada com sucesso");
					}else{ 
						$validation->addError( "Falha ao fazer o upload do arquivo"); 
					} 
				}else{ 
					$validation->addError( 'Apenas imagens no formato JPG, GIF, PNG são aceitos'); 
				} 
			}else{ 
				$validation->addError( 'Por favor selecione uma imagem'); 
			} 
		} 
		if (currentUser()) {
			$posted_values = ['fname' => currentUser()->fname, 'lname' => currentUser()->lname, 'username' => currentUser()->username, 'email' => currentUser()->email, 'position' => currentUser()->position, 'phone' => currentUser()->phone,'image' => currentUser()->image, 'b_date' => currentUser()->b_date, 'acl' =>  str_replace('"', '',currentUser()->acl), 'team' =>  str_replace('"', '',currentUser()->team)];
		}
		$this->view->post = $posted_values;
		$this->view->displayErrors = $validation->displayErrors();
		$this->view->render('register/profile');
		
	}

	public function listAction()
	{
		$this->view->render('register/list');
	}

	public function processAction()
	{
		$post_params = ($_POST['params']);
		$user = $this->UsersModel->findById($post_params);
		$this->jsonResponse($user);
	}

	public function searchAction()
	{
	}
	public function resultsAction()
	{
		$users = $this->UsersModel->find(['conditions' => 'deleted =?', 'bind' => [0]]);
		if ($users) {
			$data = array(
				"data" => $users
			);
			$this->jsonResponse($data);
		}
	}

	public function updateAction()
	{
		if ($_POST) {
			//validation
			$validation = new Validate();
			$validation->check($_POST, [
				'lname' => [
					'display' => 'Nome',
					'required' => true,
					'min' => 6
				],
				'fname' => [
					'display' => 'Sobrenome',
					'required' => true,
				],
				'email' => [
					'display' => 'Email',
					'required' => true
				],
				'phone' => [
					'display' => 'Telefone',
					'required' => true
				],
				'position' => [
					'display' => 'Cargo',
					'required' => true
				]
			]);

			if ($validation->passed()) {
				$user = new Users();

				$user->updateUser($_POST);
				//ajax response ok
			} else {
				//ajax response with errors
			}
		}
	}

	public function modalAction()
	{
		$this->view->render('register/modal');
	}
}
