<?php

class LeadsController extends Controller
{
	public function __construct($controller, $action)
	{
		parent::__construct($controller, $action);
		$this->load_model('Leads');
		$this->view->setLayout('default');
	}

	public function indexAction()
	{
		foreach (currentUser()->acls() as $a) {
			$current_user_acls[] = $a;
		};
		if (in_array("Master", $current_user_acls)) {
			$this->view->render('leads/master');
		} else {
			$this->view->render('leads/index');
		}
	}
	public function registerAction()
	{
		$post_params = explode('-', $_POST['params']);
		$origin = $post_params[0];
		$id = $post_params[1];
		$lead = new Leads();
		$table = 'leads_' . currentUser()->company_id;

		$params = [
			't1' => 'empresas',
			't2' => $table,
			'conditions' => [
				't2.consulta_id = t1.id',
				't2.deleted = 0',
				't1.deleted = 0',
				't2.id = ?'
			],
			'bind' => []
		];

		if ($origin == 'consulta') {
			//se o lead precisa ser criado
			$insertParams = [
				"user_id" => currentUser()->id,
				"user_name" => currentUser()->fname . ' ' . currentUser()->lname,
				"created_by" => currentUser()->fname . ' ' . currentUser()->lname,
				"company_id" => currentUser()->company_id,
				"consulta_id" => $id,
			];
			$lead->insertLead($table, $insertParams);
			array_push($params['bind'], $lead->lastInsert());
		}

		if ($origin == 'lead') {
			//se o lead já existe
			array_push($params['bind'], $id);
		}

		$lead = $lead->showCompanyLeads($params)[0];
		$this->jsonResponse($lead);
	}

	public function registerMasterAction()
	{
		foreach (currentUser()->acls() as $a) {
			$current_user_acls[] = $a;
		};
		if (in_array("Master", $current_user_acls)) {
			$post_params = explode('-', $_POST['params']);
			$company_id = ($_POST['id']);
			$id = $post_params[1];


			$lead = new Leads();
			$table = 'leads_' . $company_id;
			$params = [
				't1' => 'empresas',
				't2' => $table,
				'conditions' => [
					't2.consulta_id = t1.id',
					't2.deleted = 0',
					't1.deleted = 0',
					't2.id = ?'
				],
				'bind' => []
			];

			array_push($params['bind'], $id);

			$lead = $lead->showCompanyLeads($params)[0];
			$this->jsonResponse($lead);
		} else {
			$this->view->render('leads/index');
		}
	}

	public function updateAction()
	{
		if ($_POST) {
			$lead = new Leads();
			$lead->updateLead($_POST);
		}
	}

	public function resultsAction()
	{
		$current_user_acls = [];
		$params = [];
		$leads = new Leads();
		$table = 'leads_' . currentUser()->company_id;
		$params = [
			't1' => 'empresas',
			't2' => $table,
			'on_t2' => 't2.consulta_id',
			'conditions' => [
				't2.consulta_id = t1.id',
				't2.deleted = t1.deleted',
				't2.deleted = 0'
			],
			'limit' => 300,
			'order' => 't2.last_update'
		];

		foreach (currentUser()->acls() as $a) {
			$current_user_acls[] = $a;
		}


		if (in_array("Master", $current_user_acls)) {
			if (isset($_POST['company_id'])) {
				unset($_POST['company_name']);
				$table = 'leads_' . Input::get('company_id');
			} else {
				$table = 'leads_' . currentUser()->company_id;
			}

			$params = [
				't1' => 'empresas',
				't2' => $table,
				'on_t2' => 't2.consulta_id',
				'conditions' => [
					't2.consulta_id = t1.id',
					't2.deleted = t1.deleted',
					't2.deleted = 0'
				],
				'limit' => 300,
				'order' => 't2.last_update'
			];
			return $this->_masterResults($params, $leads);
		}
		if (in_array("Admin", $current_user_acls)) {

			return $this->_adminResults($params, $leads);
		}
		if (in_array("User", $current_user_acls)) {
			return $this->_userResults($params, $leads);
		}
	}
	/*
	public function resultsAction()
	{
		$params = [];
		$leads = new Leads();
		$table = 'leads_' . currentUser()->company_id;
		$params = [
			't1' => 'empresas',
			't2' => $table,
			'on_t2' => 't2.consulta_id',
			'conditions' => [
				't2.consulta_id = t1.id',
				't2.deleted = t1.deleted',
				't2.deleted = 0'
			],
			'limit' => 300,
			'order' => 't2.last_update'
		];

		if (isset($_POST) && $_POST['params'] != '') {
			if ($_POST['params'] === 'todos') {
				$params['conditions'] = [
					't2.consulta_id = t1.id',
					't2.deleted = t1.deleted',
					't2.deleted = 0'
				];
				$results = $leads->showUserLeads($params);
				$data = array(
					"data" => $results
				);
				$this->jsonResponse($data);
			}
			if ($_POST['params'] == 'abertos') {
				$params['conditions'][] =
					't2.status != "conversao"';
				$results = $leads->showUserLeads($params);
				$data = array(
					"data" => $results
				);
				$this->jsonResponse($data);
			}
			if ($_POST['params'] == 'fechados') {
				$params['conditions'][] =
					't2.status = "conversao"';
				$results = $leads->showUserLeads($params);
				$data = array(
					"data" => $results
				);
				$this->jsonResponse($data);
			}
		} else {

			$results = $leads->showUserLeads($params);
			$data = array(
				"data" => $results
			);
			$this->jsonResponse($data);
		}
	} */

	protected function _masterResults($params, $leads)
	{
		$results = $leads->showCompanyLeads($params);
		$data = array(
			"data" => $results
		);
		return $this->jsonResponse($data);
	}

	protected function _adminResults($params, $leads)
	{
		$results = $leads->showCompanyLeads($params);
		$data = array(
			"data" => $results
		);
		return $this->jsonResponse($data);
	}

	protected function _userResults($params, $leads)
	{
		$filterUser =  't2.user_id = ' . currentUser()->id;
		array_push($params['conditions'], $filterUser);
		$results = $leads->showUserLeads($params);
		$data = array(
			"data" => $results
		);
		return $this->jsonResponse($data);
	}

	public function modalAction()
	{
		$this->view->render('leads/modal');
	}

	public function resultsMasterAction()
	{
		$this->view->render('leads/resultsMaster');
	}

	public function searchAction()
	{
	}

	public function processSearchAction()
	{
		$validation = new Validate();

		if ($_POST) {

			$valid_params = $validation->validatePost($_POST);


			$validation->check($_POST, [
				'company_id' => [
					'display' => 'Nome da empresa',
					'required' => true

				]
			]);

			$table = 'leads_' . Input::get('company_id');

			if (!empty($valid_params) && $validation->passed()) {
				$leads = new Leads();
				$data['results'] = $leads->findCompanyLeads($table,  $valid_params);
				$this->jsonResponse($data['results']);
			} else if (!$validation->passed()) {
				$data['errors'] = $validation->displayErrors();
				$this->jsonResponse($data);
			}
		}
	}
}
