<?php $this->setSiteTitle('Accesso Restrito'); ?>
<?php $this->start('body'); ?>
<h1 class="text-center">Erro, accesso restrito!</h1><br>
<h4 class="text-center ">Causas possíveis:<br>classe de usuário não possui acesso,<br>controlador/action não existe,<br>erro ao declarar classe no controlador!</h3>
<?php $this->end(); ?>