<?php $this->setLayout('no_layout'); ?>
<?php $lead = $_POST['data']; ?>
<?php foreach (currentUser()->acls() as $a) {
    $current_user_acls[] = $a;
}; ?>
<div class="container">
    <?php if (in_array("Master", $current_user_acls)) { ?>
    <?php } else { ?>
        <form id="edit-lead" method="post">
        <?php } ?>
        <div class="modal-header">
            <?php if (in_array("Master", $current_user_acls)) { ?>
                <h5 class="modal-title" id="newLeadModal"><b>Consultar Leads</b></h5>
            <?php } else { ?>
                <h5 class="modal-title" id="newLeadModal"><b>Editar Lead</b></h5>
            <?php } ?>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <h3 style="padding-top: 1rem;" class="text-center  align-middle"><b>Informações Gerais da Empresa</b></h3>

        <div class=" row modal-header">

            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem; ">Nome Fantasia: &nbsp;&emsp;&nbsp;&emsp;&nbsp; &emsp;&emsp; </p>
                <p class="text-capitalize" style="margin-top: 1rem;">
                    <?= $lead['nome_fantasia'] ?>
                </p>
            </div>

            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem; ">Razão Social: &nbsp;&emsp;&nbsp;&emsp;&nbsp; &emsp;&emsp; </p>
                <p class="text-capitalize" style="margin-top: 1rem;">
                    <?= $lead['razao_social'] ?>
                </p>
            </div>
            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Cnpj: &nbsp;&emsp;&nbsp;&emsp;&nbsp; &emsp;&nbsp; &emsp;&nbsp;&nbsp;&nbsp;&emsp;&nbsp;&emsp;&emsp;</p>
                <p class="text-capitalize" style="margin-top: 1rem;">
                    <?= $lead['cnpj'] ?>
                </p>
            </div>
            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Município: &nbsp;&emsp;&emsp;&nbsp; &emsp;&emsp;&emsp;&emsp;
                </p>
                <p style="margin-top: 1rem;" name="municipio">
                    <?= $lead['municipio'] ?> &nbsp; (<?= $lead['uf'] ?>)
                </p>
            </div>

            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Telefone: &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p>
                <p class="text-capitalize" style="margin-top: 1rem;"><?= $lead['ddd_1'] ?>&nbsp;</p>
                <p class="text-capitalize" style="margin-top: 1rem;"><?= $lead['telefone_1'] ?></p>
            </div>
            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Telefone Opcional: &emsp;&emsp;&emsp;
                </p>
                <p class="text-capitalize" style="margin-top: 1rem;"><?= $lead['ddd_2'] ?>&nbsp;</p>
                <p class="text-capitalize" style="margin-top: 1rem;"><?= $lead['telefone_2'] ?></p>
            </div>
            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Email Cadastrado: &nbsp;&emsp;&nbsp;&emsp;&emsp;&nbsp;
                </p>
                <p class="text-capitalize" style="margin-top: 1rem;"><?= $lead['email'] ?>&nbsp;</p>
            </div>
            <div class="input-group col-6 border-bottom ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Data constituição: &nbsp;&emsp;&nbsp;&emsp;&emsp;
                </p>
                <p class="text-capitalize" style="margin-top: 1rem;"><?= $lead['data_inicio_ativ'] ?>&nbsp;</p>
            </div>

            <div class="input-group col-8 border-bottom  ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Localização: &emsp;&emsp;&nbsp; &emsp;&emsp;&emsp;&emsp;
                </p>
                <p style="margin-top: 1rem;">
                    <?= $lead['tipo_logradouro'] ?>&nbsp;<?= $lead['logradouro'] ?>&nbsp;-&nbsp;nº&nbsp;<?= $lead['numero'] ?>&nbsp;-&nbsp; <?= $lead['bairro'] ?>
                </p>
            </div>
            <div class="input-group col-4 border-bottom  ">
                <p class="text-secondary text-info" style="margin-top: 1rem;">Complemento:&emsp;
                </p>
                <p style="margin-top: 1rem;">
                    <?= $lead['complemento'] ?>
                </p>
            </div>
            <div class="modal-body">

                <div class="form-row">
                    <div class="col-3 d-none">
                        <input type="text" name="created_by" id="created_by" value="" hidden>
                        <input type="text" name="id" id="id" value="" hidden>
                        <input type="text" name="user_id" id="user_id" value="" hidden>
                        <input type="text" name="company_id" id="company_id" value="" hidden>
                        <input type="text" name="consulta_id" id="consulta_id" value="" hidden>
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">Usuário Responsável</span>
                        <input type="text" class="form-control" name="user_name" id="user_name" value="<?= $lead['user_name'] ?>">
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">Nome Responsável (Empresa)</span>
                        <input type="text" class="form-control" name="contact_name" id="contact_name" value="<?= $lead['contact_name'] ?>">
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">Cargo Responsável</span>
                        <input type="text" class="form-control" name="contact_role" id="contact_role" value="<?= $lead['contact_role'] ?>">
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">Telefone Responsável</span>
                        <input type="text" class="form-control" name="contact_phone" id="contact_phone" value="<?= $lead['contact_phone'] ?>">
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">E-mail Responsável</span>
                        <input type="text" class="form-control" name="contact_email" id="contact_email" value="<?= $lead['contact_email'] ?>">
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">Estado do último contato</span>
                        <input style="display: none;" type="text" class="form-control" name="last_contact_status" id="last_contact_status" value="<?= $lead['last_contact_status'] ?>">
                        <select class="custom-select" id="last_contact_status_select">
                            <option value="semcontato">Sem contato</option>
                            <option value="desconhecido">Desconhecido</option>
                            <option value="indisponivel">Indisponível</option>
                            <option id="atendido" value="atendido">Atendido</option>
                            <option id="retorno" value="retorno">Solicitou retorno</option>
                        </select>
                    </div>
                    <div class="col-3 text-success">
                        <span style="padding-right: 10px;">Data do último contato</span>
                        <input disabled type="text" class="form-control " name="last_contact_time" id="last_contact_time" value="<?= $lead['last_contact_time'] ?>">
                    </div>
                    <div class="col-2 text-success">
                        <span style="padding-right: 10px;" for="status">Etapa</span>
                        <input style="display: none;" type="text" class="form-control" name="status" id="status" value="<?= $lead['status'] ?>">
                        <select class="custom-select" id="status_select">
                            <option value="prospeccao">Prospecção</option>
                            <option value="qualificado">Qualificado</option>
                            <option value="proposta">Proposta</option>
                            <option value="negociacao">Negociação</option>
                            <option id="conversao" value="conversao">Conversão</option>
                        </select>
                    </div>
                    <div class="col-1 text-success" id="conversion-group" style="display: none;">
                        <span style="padding-right: 10px;" for="status">Selecione</span>
                        <input style="display: none;" type="text" class="form-control" name="conversion" id="conversion" value="<?= $lead['conversion'] ?>">
                        <select class="custom-select" id="conversion_select">
                            <option value="sim">Sim</option>
                            <option value="nao">Não</option>
                        </select>
                    </div>
                    <div class="col-12 text-success">
                        <span style="padding-right: 10px;">Observações</span>
                        <textarea class="form-control" name="notes" id="notes"><?= $lead['notes'] ?></textarea>
                    </div>

                </div>
            </div>

            <br>
        </div>
        <div class="modal-footer">
            <h6 class="mr-auto text-info" id="newLeadModal">Criado por: <?= $lead['created_by'] ?>, em <?= $lead['created_at'] ?></h6>
            <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
            <?php if (in_array("Master", $current_user_acls)) { ?>
            <?php } else { ?>
                <input class="btn btn-default btn-success" type="submit" name="submit" value="Salvar" id="saveLead">

        </div>
        </form>

    <?php } ?>
</div>


<?php if (in_array("Master", $current_user_acls)) { ?>
<?php } else { ?>
    <script>
        $(document).ready(function() {
            var created_by = "<?= $lead['created_by'] ?>";
            var lead_id = "<?= $lead['id'] ?>";
            var user_id = "<?= $lead['user_id'] ?>";
            var company_id = "<?= $lead['company_id'] ?>";
            var consulta_id = "<?= $lead['consulta_id'] ?>";

            var table = $('#leads-table').DataTable();
            var last_contact_status = $('#last_contact_status').val();
            var status = $('#status').val();
            var conversion = $('#conversion').val();
            var date = $('#last_contact_time').val();

            if (last_contact_status == '') {
                last_contact_status = 'semcontato';
                $('#last_contact_status').val(last_contact_status);
            }

            if (date == '') {
                var newdate = new Date().toLocaleString();
                $('#last_contact_time').val(newdate);
            }

            if ($("#status").val() == 'conversao') {
                $("#conversion-group").css({
                    display: 'block'
                });
            }

            if (status == '') {
                status = 'prospeccao';
                $('#status').val(status);
            }

            $('#last_contact_status_select').val(last_contact_status).change();
            $('#status_select').val(status).change();
            $('#conversion_select').val(conversion).change();

            $("#last_contact_status_select").on({
                "change": function() {
                    $("#last_contact_status").val($("#last_contact_status_select").val());
                    var date = new Date().toLocaleString();
                    $('#last_contact_time').val(date);
                }
            });

            $("#status_select").on({
                "change": function() {
                    $("#status").val($("#status_select").val());
                    if ($("#status").val() == 'conversao') {
                        $("#conversion-group").css({
                            display: 'block'
                        });
                        $('#conversion_select').val("sim").change();
                    } else {
                        $("#conversion-group").css({
                            display: 'none'
                        });
                        $('#conversion').val("");
                    }
                }
            });

            $("#last_contact_status_select").on({
                "change": function() {

                    if ($("#last_contact_status_select").val() == 'atendido' || $("#last_contact_status_select").val() == 'retorno') {
                        $("#conversao").css({
                            display: ''
                        });

                    } else {
                        $("#conversao").css({
                            display: 'none'
                        });
                    }
                }
            });

            $("#conversion_select").on({
                "change": function() {
                    $("#conversion").val($("#conversion_select").val());
                }
            });

            $("#edit-lead").submit(function(e) {
                blockUi('#modal-content');
                $('#created_by').val(created_by);
                $('#id').val(lead_id);
                $('#user_id').val(user_id);
                $('#company_id').val(company_id);
                $('#consulta_id').val(consulta_id);

                $('#last_contact_time').prop("disabled", false);

                e.preventDefault();
                var $form = $(this);
                var serializedData = $form.serialize();
                $.ajax({
                    url: '<?= PROOT ?>leads/update',
                    type: 'POST',
                    data: serializedData,
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function(response) {
                        $('body').removeClass('modal-open')
                        $('.modal-backdrop').remove()
                        $('#search_form').submit();
                        $('#editLead').modal('hide').on("hidden.bs.modal", function(e) {
                            e.preventDefault();
                            $('#leads-table').DataTable().ajax.reload();
                        });
                    },
                    error: function() {
                        alert("error");
                    }
                });


            });
            $('.modal').on('hidden.bs.modal', function() {
                $('#leads-table tbody').unblock()
            })

        });
    </script>

<?php } ?>