<?php $this->setLayout('no_layout'); ?>

<style>
  .table td,
  .table td,
  .table th {
    padding: .25rem !important;
  }

  #results {
    justify-content: center;
    display: flex;
  }

  #company-search_length {
    margin-bottom: -3rem !important;
    padding-left: 3rem !important;
  }
</style>
<div class="row display-errors justify-content-center">
  <?php if (isset($_POST['results']['errors'])) : ?>
    <h5 class="text-danger"><?= $_POST['results']['errors'] ?></h5>
  <?php endif ?>
</div>
<div class="col-md-12 justify-content-center">
  <?php if (isset($_POST['results']) && !isset($_POST['results']['empty'])) :
    $resultado = $_POST['results']; ?>


    <table class="table col-md-12 table-borderless" id="company-search">
      <thead class="thead-dark">
        <tr>
          <?php foreach (searchAccess('Leads', 'results_params') as $k => $v) : ?>
            <th>
              <?= $k ?>
            </th>
          <?php endforeach ?>
          <th>Ver Info</th>
        </tr>
      </thead>
      <tbody>
        <?php if ($resultado == 'false') { ?>
          <div class=" text-center text-danger">
            <h4 class="text-center">Essa empresa não possúi Leads!</h4>
          </div>
        <?php } else { ?>

          <?php foreach ($resultado as $r) : ?>
            <tr id="<?= 'consulta-' . $r['id'] ?>">
              <?php foreach (searchAccess('Leads', 'results_params') as $key => $value) : ?>
                <td style=" font-size:12px; max-width: 25ch;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                  <?= $r[$value] ?>
                </td>

              <?php endforeach ?>
              <td class="text-center"><button type="button" class=" btn btn-outline-primary btn-sm btn-consultas" data-toggle="modal" data-target="#editLead" id="consulta-<?= $r['id'] ?>">Ver</button></td>
            </tr>
          <?php endforeach ?>
        <?php } ?>
      </tbody>
    </table>
  <?php elseif (!isset($_POST['results']['errors'])) : ?>
    <div class="row">
      <h4>Sem resultados!</h4>
    </div>
  <?php endif ?>
</div>
<!-- Modal -->
<div class="modal fade" id="editLead" tabindex="-1" role="dialog" aria-labelledby="newLeadModal" aria-hidden="true">
  <div class="modal-xl modal-dialog" role="document">
    <div class="modal-content" id="modal-content">

    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('#company-search').DataTable();
  })
</script>

<script>
  var table = $('#company-search').DataTable();
  $('#company-search tbody').on('click', 'button', function() {
    var data = table.row($(this).parents('tr')).data();

    var id = $("#company_id").val();
    var params = data.DT_RowId;
    $.ajax({
      url: '<?= PROOT ?>leads/registerMaster',
      type: "POST",
      data: {
        params,
        id
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      success: function(data) {
        blockUi('#modal-content')
        $('.modal-content').load(
          '<?= PROOT ?>leads/modal', {
            data
          }
        );
      },
      error: function() {
        alert('Something went wrong!');
      }

    })
  });
  $('.modal').on('hidden.bs.modal', function() {
    $('#leads-table tbody').unblock()
  })
</script>