<?php $this->start('head'); ?>
<?php $this->end(); ?>
<?php $this->setSiteTitle('Leads'); ?>
<?php $this->start('body'); ?>

<section>
    <?php foreach (currentUser()->acls() as $a) {
        $current_user_acls[] = $a;
    };
    if (in_array("Admin", $current_user_acls)) { ?>
        <h4 class="text-center pb-5"><b>Leads da Empresa </b></h4>
    <?php } else { ?>
        <h4 class="text-center pb-5"><b>Meus Leads</b></h4>
    <?php  } ?>
    <div id="hidden d-none">
        <div class="row ">
            <!--<div class="col-2" id="select-box">

                <select class="form-control" id="results_select">
                    <option value="todos">Todos</option>
                    <option value="abertos">Em aberto</option>
                    <option value="fechados">Encerrados</option>
                </select>
            </div>-->
        </div>
        <div class="col-md-12" id="resultado">
            <table class="table col-md-12 table-borderless " id="leads-table" data-order='[[ 0, "asc" ]]' data-page-length='25'>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Nome Fantasia</th>
                        <th id="creator" scope="col">Responsável</th>
                        <th id="contactName" scope="col">Nome do Contato</th>
                        <th scope="col">Estado</th>
                        <th id="lastContact" scope="col">Último contato</th>
                        <th id="date" scope="col">Data</th>
                        <th scope="col">Conversão</th>
                        <th scope="col">Edição</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

        <div class="modal fade" id="editLead" tabindex="-1" role="dialog" aria-labelledby="newLeadModal" aria-hidden="true">
            <div class="modal-xl modal-dialog" role="document">
                <div class="modal-content" id="modal-content">

                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $("#leads-table").DataTable({
            // "scrollY": "800px",
            // "paging": false,
            "processing": true,
            //"serverSide": true,
            "ajax": {
                "url": "results",
                "type": "POST",
            },
            "columns": [{
                "data": "razao_social"
            }, {
                "data": "user_name"
            }, {
                "data": "contact_name"
            }, {
                "data": "status"
            }, {
                "data": "last_contact_time"
            }, {
                "data": "last_contact_status"
            }, {
                "data": "conversion"
            }, {
                "defaultContent": "<button class='btn-leads btn-outline-primary btn' id='++' data-toggle='modal' data-target='#editLead'>ver</button>"
            }]
        });


        var table = $('#leads-table').DataTable();
        $('#leads-table tbody').on('click', 'button', function() {
            var data = table.row($(this).parents('tr')).data();
            var params = "lead-" + data.id
            blockUi('#leads-table tbody');
            $.ajax({
                url: '<?= PROOT ?>leads/register',
                type: "POST",
                data: {
                    params
                },
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data) {
                    blockUi('#modal-content')
                    $('.modal-content').load(
                        '<?= PROOT ?>leads/modal', {
                            data
                        }
                    );
                },
                error: function() {
                    alert('Something went wrong!');
                }
            });

        });
    });
</script>

<?php $this->end(); ?>