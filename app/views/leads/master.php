<?php $this->start('head'); ?>
<?php $this->end(); ?>
<?php $this->setSiteTitle('Leads'); ?>
<?php $this->start('body'); ?>

<style>
  .invalid-feedback {
    display: none;
  }
</style>
<section>

  <h4 class="text-center pb-5"><b>Consultar Leads</b></h4>
  <?php foreach (currentUser()->acls() as $a) {
    $current_user_acls[] = $a;
  };
  if (in_array("Master", $current_user_acls)) { ?>
    <div class="row text-center">
      <div class="container mb-4 d-flex justify-content-center  mr-auto ml-auto">
        <form class="form-inline col-12" id='filterCompany'>
          <div class="form-group col-12 justify-content-center">
            <label for="company_name " class="input-group-text mr-2 ">Pesquisar</label>
            <input type="text" id="company_name" class="form-control col-8" name="company_name" value="" autocomplete="off">

            <button class="btn btn-outline-primary ml-2 col-1" type="submit" id="enviar">enviar</button>
          </div>
          <input type="text" hidden id="company_id" name="company_id" class="form-control" value="<?= $_POST['company_id'] ?>">
        </form>
      </div>
    </div>
    <div id="suggesstion-box" class="col-5  ml-auto mr-auto" for="company_name" style="margin-top: -1.5rem;" data-aos="fade">
      <ul id="company-list" class="list-group"></ul>
    </div>
  <?php } ?>
  <div id="errorMessage" style="padding-bottom: 20px;" class="mp-12 container text-center d-none mt-4">
    <div class="list-group-item list-group-item-danger text-center ">

    </div>
  </div>

  <div class="col-md-12" id="results">
  </div>
</section>

<script type="text/javascript">
  var validator = $('#filterCompany').validate({
    errorLabelContainer: "#errorMessage div",
    wrapper: "div",
    rules: {
      company_name: {
        required: true,
        minlength: 1,
        maxlength: 50
      },
    },
    messages: {
      company_name: {
        required: 'Insira o nome da empresa',
        minlength: 'Nome da empresa muito curta, insira corretamente',
        maxlength: 'Nome da empresa muito longa, insira corretamente'
      }
    }
  });


  $(document).ready(function() {
    $('#footer').attr('style', 'margin-top: 21rem !important;');
    $("#filterCompany").submit(function(e) {
      e.preventDefault();
      var $form = $(this);
      var invalid = $('.invalid-feedback')
      invalid.remove
      var serializedData = $form.serialize();
      if (validator.errorList.length != 0) {
        $("#errorMessage").removeClass("d-none");
        return false
      } else {
        $.ajax({
          url: '<?= PROOT ?>leads/processSearch',
          type: "POST",
          data: serializedData,
          contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
          success: function(results) {
            $('#results').load(
              'resultsMaster', {
                results
              }
            );
            $('.invalid-feedback').add('hidden', 'true');
            $('#footer').removeAttr('style', 'margin-top: 21rem !important;')
          },
          error: function() {
            $('#results').load(
              '<?= PROOT ?>restricted/error'
            );
          }
        });
      }
    });


  });

  function autoComplete() {
    $("#company_name").keyup(function() {
      $.ajax({
        type: "POST",
        url: "<?= PROOT ?>dashboard/autoCompleteCompany",
        data: 'keyword=' + $(this).val(),
        success: function(data) {
          $("#company-list").empty();
          $("#suggesstion-box").css("background", "#FFF");
          $("#company-list").append('<span class="dropdown text-center text-success"">Selecione</span>');
          for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            $("#company-list").append('<li class="dropdown-item" id="' + obj.id + '">' + obj.razao_social + '</li>');
            $("#suggesstion-box").show();
          }
          $click = $('#company-list li').click(function() {
            selectCompany($(this).text());
            setCompanyId($(this).attr("id"));
          });

        }
      });
    });
  }

  //To select company name
  function selectCompany(val) {
    $("#company_name").val(val);
    $("#suggesstion-box").hide();

  }
  $(document).on("click", "#company_name", function(e) {
    $(this).val('');
    $("#company_id").val('');
    autoComplete();
  });

  function setCompanyId(name) {
    $('#company_id').val(name);
  }
</script>


<?php $this->end(); ?>