<?php $this->setLayout('default'); ?>
<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Nova Empresa'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<style>
    #telefone1-error {
        position: absolute;
        margin-top: 2.3rem;
    }

    #ddd1-error {
        display: none !important;
    }
</style>

<div class="col-md-10 offset-md-1 well" id="user-register">
    <h3 class="text-center">Registro de Nova Empresa!</h3>
    <hr>
    <form class="form" method="POST" id="registerCompany">
        <div class="display-errors">
            <?= $this->displayErrors; ?>
        </div>
        <div class="form-row">
            <div class="col-4 text-info  p-md-1">
                <label for="razao_social">Razão Social</label>
                <input type="text" id="razao_social" pattern="[a-z\s]+$" name="razao_social" class="form-control" value="">
            </div>
            <div class="col-4 text-info p-md-1">
                <label for="nome_fantasia">Nome fantasia</label>
                <input type="text" id="nome_fantasia" name="nome_fantasia" class="form-control" value="">
            </div>
            <div class="col-4 text-info p-md-1">
                <label for="cnpj">CNPJ</label>
                <input type="text" id="cnpj" name="cnpj" class="form-control" pattern="[0-9]+$" value="">
            </div>
            <div class="col-3 text-info p-md-1">
                <label for="nome_responsavel">Nome Responsável</label>
                <input type="text" id="nome_responsavel" name="nome_responsavel" class="form-control" value="">
            </div>
            <div class="col-5 text-info p-md-1">
                <label for="localizacao">Endereço</label>
                <input type="text" id="localizacao" name="localizacao" class="form-control" value="">
            </div>
            <div class="col-2 text-info p-md-1">
                <label for="cep">CEP</label>
                <input type="tel" pattern="[0-9]+$" id="cep" name="cep" class="form-control" value="">
            </div>
            <div class="col-1 text-info p-md-1">
                <label for="uf">UF</label>
                <input type="text" pattern="[a-z\s]+$" maxlength="2" id="uf" name="uf" class="form-control" value="" onkeypress="return ApenasLetras(event,this)">
            </div>
            <div class="col-1 text-info p-md-1">
                <label for="municipio">Múnicipio</label>
                <input type="text" id="municipio" name="municipio" class="form-control" value="">
            </div>
            <div class="col-3 text-info p-md-1">
                <label for="ddd1">Telefone</label>
                <div class="d-flex">
                    <input type="tel" maxlength="2" id="ddd1" name="ddd1" class="form-control col-3" placeholder="ddd">
                    <input type="tel" pattern="\([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}$" maxlength="9" id="telefone1" name="telefone1" class="form-control col-9" value="">
                </div>
            </div>
            <div class="col-3 text-info p-md-1">
                <label for="ddd2">Telefone Alternativo</label>
                <div class="d-flex">
                    <input type="tel" maxlength="2" id="ddd2" name="ddd2" class="form-control col-3" value="" placeholder="ddd">
                    <input type="tel" maxlength="9" id="telefone2" name="telefone2" class="form-control col-9" value="">
                </div>
            </div>
            <div class="col-3 text-info p-md-1">
                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" class="form-control" value="">
            </div>

            <div class="col-3 text-info p-md-1">
                <label for="company_acl">Permissões</label>
                <input type="text" id="company_acl" name="company_acl" class="form-control" value="">
            </div>
            <div class="form-row">
                <div class="col-12 text-center mx-auto" id="message"></div>
            </div>
        </div>
        <div class="text-center p-md-3">
            <button id="submit-companies" type="submit" class="btn btn-outline-primary btn-large">Registrar</button>
        </div>


    </form>
</div>

<script>
    var validator = $("#registerCompany").validate({

        rules: {
            razao_social: {
                required: true,
                minlength: 5,
            },
            cnpj: {
                required: true,
                number: true,
                minlength: 9
            },
            nome_fantasia: {
                required: true,
                minlength: 5
            },
            nome_responsavel: {
                required: true,
                minlength: 3
            },
            localizacao: {
                required: true,
                minlength: 5
            },
            cep: {
                required: true,
                minlength: 8,
                digits: true
            },
            uf: {
                required: true,
                maxlength: 2
            },
            municipio: {
                required: true,
                minlength: 3,
            },
            ddd1: {
                required: true,
                maxlength: 2,
                minlength: 2
            },
            telefone1: {
                required: true,
                minlength: 8,
                maxlength: 9
            }
        },
        messages: {
            razao_social: {
                required: "Esse campo é obrigatório",
                minlength: 'Nome muito curto, insira corretamente',
            },
            cnpj: {
                required: 'Esse campo é obrigatório',
                number: 'insira apenas números',
                minlength: 'Necessário inserir 9 números',
            },
            nome_fantasia: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
            },
            nome_responsavel: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente'
            },
            localizacao: {
                required: 'Esse campo é obrigatório',
                minlength: 'Endereço muito curto, insira corretamente'
            },
            cep: {
                required: 'Esse campo é obrigatório',
                minlength: 'Cep inválido, insira corretamente',
                digits: 'insira apenas digitos'

            },
            uf: {
                required: 'Esse campo é obrigatório',
                maxlength: 'insira apenas as inciais'
            },
            municipio: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente'
            },
            ddd1: {
                required: 'Esse campo é obrigatório',
                minlength: 'DDD muito curto, insira corretamente',
                maxlength: 'DDD muito Longo, insira corretamente'
            },
            telefone1: {
                required: 'Esse campo é obrigatório',
                minlength: 'Telefone muito curto, insira corretamente',
                maxlength: 'Telefone muito Longo, insira corretamente'
            }
        }
    });

    function ApenasLetras(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            } else if (e) {
                var charCode = e.which;
            } else {
                return true;
            }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123 || charCode == 32))
                return true;
            else
                return false;
        } catch (err) {
            alert(err.Description);
        }
    }
</script>

<?php $this->end(); ?>