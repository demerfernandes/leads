<?php $this->setLayout('no_layout'); ?>
<?php $company = $_POST['data']; ?>
<style>
    #telefone1-error {
        position: absolute;
        margin-top: 2.3rem;
    }

    #ddd1-error {
        display: none !important;
    }
</style>

<div class="container">
    <form id="edit-company" method="post">
        <div class="modal-header">
            <h5 class="modal-title" id="newLeadModal"><b>Editar Empresa</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <input required hidden type="text" id="id" name="id" value="<?= $company['id'] ?>">
            <div class="form-row">
                <div class="col-3 text-info">
                    <label for="razao_social">Razão social</label>
                    <input required type="text" id="razao_social" name="razao_social" class="form-control" value="<?= $company['razao_social'] ?>">
                </div>

                <div class="col-3 text-info">
                    <label for="nome_fantasia">Nome Fantasia</label>
                    <input type="text" id="nome_fantasia" name="nome_fantasia" class="form-control" value="<?= $company['nome_fantasia'] ?>">
                </div>
                <div class="col-3 text-info">
                    <label for="cnpj">CNPJ</label>
                    <p id="cnpj" class="form-control"><?= $company['cnpj'] ?></p>
                </div>
                <div class="col-3 text-warning">
                    <label for="company_acl">Permissões</label>
                    <input type="text" id="company_acl" name="company_acl" class="form-control" value="<?= $company['company_acl'] ?>" readonly>
                </div>
                <div class="col-3 text-info">
                    <label for="nome_responsavel">Nome Responsável</label>
                    <input required type="text" id="nome_responsavel" name="nome_responsavel" class="form-control" value="<?= $company['nome_responsavel'] ?>">
                </div>
                <div class="col-3 text-info">
                    <label for="localizacao">Endereço</label>
                    <input required type="text" id="localizacao" name="localizacao" class="form-control" value="<?= $company['localizacao'] ?>">
                </div>
                <div class="col-3 text-info">
                    <label for="cep">CEP</label>
                    <input required type="number" id="cep" name="cep" class="form-control" value="<?= $company['cep'] ?>">
                </div>
                <div class="col-3 text-info">
                    <label for="uf">UF</label>
                    <input required type="text" id="uf" name="uf" class="form-control" value="<?= $company['uf'] ?>">
                </div>
                <div class="col-3 text-info">
                    <label for="municipio">Múnicipio</label>
                    <input required type="text" id="municipio" name="municipio" class="form-control" value="<?= $company['municipio'] ?>">
                </div>

                <div class="col-3 text-info">
                    <label for="ddd1">Telefone</label>
                    <div class="d-flex">
                        <input required type="tel" maxlength="2" id="ddd1" name="ddd1" class="form-control col-3" value="<?= $company['ddd1'] ?>" placeholder="(47)">
                        <input required type="tel" id="telefone1" name="telefone1" class="form-control col-9" value="<?= $company['telefone1'] ?>">
                    </div>
                </div>

                <div class="col-3 text-info">
                    <label for="ddd2">Telefone Alternativo</label>
                    <div class="d-flex">
                        <input type="tel" maxlength="2" id="ddd2" name="ddd2" class="form-control col-3" value="<?= $company['ddd2'] ?>" placeholder="(47)">
                        <input type="tel" id="telefone2" name="telefone2" class="form-control col-9" value="<?= $company['telefone2'] ?>">
                    </div>
                </div>
                <div class="col-3 text-info">
                    <label for="email">E-mail</label>
                    <input type="email" id="email" name="email" class="form-control" value="<?= $company['email'] ?>">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
            <input class="btn btn-default btn-success" type="submit" name="submit" value="Salvar">
        </div>
    </form>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $("#edit-company").submit(function(e) {
            e.preventDefault();
            var $form = $(this);
            var serializedData = $form.serialize();
            if (validator.errorList.length >= 1) {
                return false;
            } else {
                blockUi('#edit-company');
                $.ajax({
                    url: 'update',
                    type: 'POST',
                    data: serializedData,
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function(response) {
                        $("[data-dismiss=modal]").trigger({
                            type: "click"
                        });

                    },
                    error: function() {
                        alert("error");
                    }
                });
            }
        });
        $("#edit-company").on("submit", function() {
            $('#table-companies').DataTable().ajax.reload();
        });

        $('.modal').on('hidden.bs.modal', function() {
            $('#table-companies tbody').unblock()
        })
    });
    var validator = $("#edit-company").validate({

        rules: {
            razao_social: {
                required: true,
                minlength: 5,
            },
            nome_fantasia: {
                required: true,
                minlength: 5
            },
            nome_responsavel: {
                required: true,
                minlength: 3
            },
            localizacao: {
                required: true,
                minlength: 5
            },
            cep: {
                required: true,
                minlength: 8,
                digits: true
            },
            uf: {
                required: true,
                maxlength: 2
            },
            municipio: {
                required: true,
                minlength: 3,
            },
            ddd1: {
                required: true,
                maxlength: 2,
                minlength: 2
            },
            telefone1: {
                required: true,
                minlength: 8,
                maxlength: 9
            }
        },
        messages: {
            razao_social: {
                required: "Esse campo é obrigatório",
                minlength: 'Nome muito curto, insira corretamente',
            },
            nome_fantasia: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
            },
            nome_responsavel: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente'
            },
            localizacao: {
                required: 'Esse campo é obrigatório',
                minlength: 'Endereço muito curto, insira corretamente'
            },
            cep: {
                required: 'Esse campo é obrigatório',
                minlength: 'Cep inválido, insira corretamente',
                digits: 'insira apenas digitos'

            },
            uf: {
                required: 'Esse campo é obrigatório',
                maxlength: 'insira apenas as inciais'
            },
            municipio: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente'
            },
            ddd1: {
                required: 'Esse campo é obrigatório',
                minlength: 'DDD muito curto, insira corretamente',
                maxlength: 'DDD muito Longo, insira corretamente'
            },
            telefone1: {
                required: 'Esse campo é obrigatório',
                minlength: 'Telefone muito curto, insira corretamente',
                maxlength: 'Telefone muito Longo, insira corretamente'
            }
        }
    });
</script>