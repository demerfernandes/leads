<?php $this->setLayout('default'); ?>
<?php $this->setSiteTitle('Empresas'); ?>
<?php $this->start('body'); ?>

<h3 class="text-center empresas">Lista de Empresas</h3>
<style>
    #table-companies_length {
        margin-bottom: -3rem;
        padding-left: 3rem;
    }
</style>

<div class="col-md-12" id="resultado">
    <div class="" id="results">

        <table class="table col-md-12 table-borderless" id="table-companies">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">CNPJ</th>
                    <th scope="col">Objeto Social</th>
                    <th scope="col">Acesso</th>
                    <th scope="col">Nome Responsável</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Email</th>
                    <th scope="col">Edição</th>
                </tr>
            </thead>


        </table>

    </div>
</div>

</div>
<!-- Modal -->
<div class="modal fade" id="editCompany" tabindex="-1" role="dialog" aria-labelledby="newLeadModal" aria-hidden="true">
    <div class="modal-xl modal-dialog" role="document">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#table-companies').DataTable({
            // "scrollY": "800px",
            // "paging": false,
            "processing": true,
            //"serverSide": true,
            "ajax": {
                "url": "process",
                "type": "POST",
            },
            "columns": [{
                "data": "razao_social"
            }, {
                "data": "cnpj"
            }, {
                "data": "nome_fantasia"
            }, {
                "data": "company_acl"
            }, {
                "data": "nome_responsavel"
            }, {
                "data": "telefone1"
            }, {
                "data": "email"
            }, {
                "defaultContent": "<button class='btn-leads btn-outline-primary btn' data-toggle='modal' data-target='#editCompany'>ver</button>"
            }]
        });
        /*       var button = $('button.toggle-vis').on( 'click', function (e) {
                    e.preventDefault();
                    if (button === button){
                        button.addClass('active');
                    }



                    // Get the column API object
                    var column = table.column( $(this).attr('data-column') );

                    // Toggle the visibility
                    column.visible( ! column.visible() );
                } ); */

        var table = $('#table-companies').DataTable();
        var response = $('#table-companies tbody').on('click', 'button', function() {
            var data = table.row($(this).parents('tr')).data();
            var params = data.id;
            blockUi('#table-companies tbody');
            $.ajax({
                url: 'search',
                type: "POST",
                data: {
                    params
                },
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data) {
                    blockUi('.modal-content');
                    $('.modal-content').load(
                        'modal', {
                            data
                        }
                    );
                },
                error: function() {
                    alert('Something went wrong!');
                }
            });

        });



    });
</script>

<?php $this->end(); ?>