<?php $this->setLayout('no_layout'); ?>

<style>
    .table td,
    .table td,
    .table th {
        padding: .25rem !important;
    }

    #lead-search_length {
        margin-bottom: -3rem !important;
        padding-left: 3rem !important;
    }
</style>
<div class="row display-errors justify-content-center">
    <?php if (isset($_POST['results']['errors'])) : ?>
        <h5 class="text-danger"><?= $_POST['results']['errors'] ?></h5>
        <script>
            $(document).ready(function() {
                $('#resultsTable').hide()

            });
        </script>
    <?php endif ?>
</div>
<div class="col-md-12 justify-content-center" id="resultsTable">
    <?php if (isset($_POST['results']) && !isset($_POST['results']['empty'])) :
        $resultado = $_POST['results']; ?>

        <table class="table col-md-12 table-borderless" id="lead-search" data-order='[[ 0, "asc" ]]' data-page-length='25'>
            <thead class="thead-dark">
                <tr>
                    <?php foreach (searchAccess('Empresas', 'results_params') as $k => $v) : ?>
                        <th>
                            <?= $k ?>
                        </th>
                    <?php endforeach ?>
                    <th>Ver Info</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($resultado as $r) : ?>
                    <tr id="<?= 'consulta-' . $r['id'] ?>">
                        <?php foreach (searchAccess('Empresas', 'results_params') as $key => $value) : ?>
                            <td style=" font-size:12px; max-width: 25ch;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">
                                <?= $r[$value] ?>
                            </td>

                        <?php endforeach ?>
                        <td class="text-center"><button type="button" class=" btn btn-outline-primary btn-sm btn-consultas" data-toggle="modal" data-target="#editLead" id="consulta-<?= $r['id'] ?>">Criar</button></td>
                    </tr>
                <?php endforeach ?>

            </tbody>
        </table>
    <?php elseif (!isset($_POST['results']['errors'])) : ?>
        <div class="row">
            <h4>Sem resultados!</h4>
        </div>
    <?php endif ?>
</div>
<!-- Modal -->
<div class="modal fade" id="editLead" tabindex="-1" role="dialog" aria-labelledby="newLeadModal" aria-hidden="true">
    <div class="modal-xl modal-dialog" role="document">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        var table = $('#lead-search').DataTable();
        $('#lead-search tbody').on('click', 'button', function() {
            blockUi('#resultsTable');
            var data = table.row($(this).parents('tr')).data();
            var params = data.DT_RowId
            table
                .row($(this).parents('tr'))
                .remove()
                .draw();
            $.ajax({
                url: '<?= PROOT ?>leads/register',
                type: "POST",
                data: {
                    params
                },
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data) {
                    blockUi('.modal-content');
                    $('.modal-content').load(
                        '<?= PROOT ?>leads/modal', {
                            data
                        }
                    );
                },
                error: function() {
                    alert('Este lead Já foi criado!');
                }
            });


        });
        $('.modal').on('hidden.bs.modal', function() {
            $('#lead-search tbody').unblock()
            $('#resultsTable').unblock()

        })
    });
</script>