<?php $this->setSiteTitle('Pesquisa'); ?>
<?php $this->start('head'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>

<style>
    #results {
        margin-left: auto;
        margin-right: auto;
    }
</style>

<div class="row text-center mx-auto" data-aos="fade-left">
    <h4>Pesquisar por:</h4>
</div>
<form id="search_form" class="needs-validation" novalidate data-aos="fade-up">
    <div class="form-row ">
        <?php foreach (searchAccess('Empresas', 'search_params') as $key => $val) : ?>
            <div class="d-block form-row col-4 mb-3 mx-auto ">
                <div class=" input-group ">
                    <div class=" input-group-text col-4 ">
                        <span style="padding-right: 10px; "><?= $key ?></span>
                    </div>
                    <label class="col-2 form-control" id="location-<?= $key ?>" hidden></label>
                    <input type="text " autocomplete="off" class="form-control" name="<?= $val ?>" id="<?= $val ?>">

                </div>

            </div>
        <?php endforeach ?>
    </div>
    <div class="form-group text-center">
        <input id="buscar" type="submit" value="Buscar" class="btn btn-outline-primary">
    </div>
</form>
<div id="errorMessage" data-aos="fade-up" style="padding-bottom: 20px;" class="mp-12 container text-center d-none">
    <div id="error" class="list-group-item list-group-item-danger text-center ">

    </div>
</div>

<div class="col-md-12" id="results">
</div>




<script>
    var cep = $('#cep').focusout(function() {
        $('#tLogradouro').remove();
        $('.text-left').remove();
        $('#location-Logradouro').attr('hidden');
        $('#errorMessage').addClass("d-none");
        if ($('#cep').val().length == 8) {
            $.ajax({
                url: 'https://viacep.com.br/ws/' + $('#cep').val() + '/json/',
                type: 'get',
                dataType: 'json',
                crossDomain: true,
                success: function(res) {
                    vazio = '';
                    if (!res.erro == true) {

                        tipoLogradouro = res.logradouro.split(' ')[0];
                        logradouro = res.logradouro.replace(tipoLogradouro, '');
                        $('#municipio').val(res.localidade);
                        $('#logradouro').val(logradouro.normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
                        var item = $('<p id="tLogradouro">' + tipoLogradouro + '</p>');
                        $('#location-Logradouro').append(item).removeAttr('hidden');
                    } else {
                        message = "<p class='text-left error'> Cep não foi encontrado </p>"
                        $('#municipio').val(vazio);
                        $('#logradouro').val(vazio)
                        $('#tLogradouro').remove();
                        $('#errorMessage').removeClass("d-none");
                        $('#error').append(message);
                        $('#location-Logradouro').attr('hidden');
                    }
                },
            })
        }
    });

    $(document).ready(function() {

        $('#footer').attr('style', 'margin-top: 21.2rem !important;');
        // $('#municipio').attr('onkeypress', 'return ApenasLetras(event,this)');
        $('#cnpj').attr('onkeypress', 'return ApenasNumeros(event,this)');
        $('#cep').attr('onkeypress', 'return ApenasNumeros(event,this)');
        $("#search_form").submit(function(e) {
            $(".is-invalid").removeClass("is-invalid");
            $(".invalid-feedback").remove();
            e.preventDefault();
            var $form = $(this);
            var serializedData = $form.serialize();
            if (validator.numberOfInvalids() >= 1) {
                $("#errorMessage").removeClass("d-none");
                return false;
            } else {
                blockUi('#results');
                $.ajax({
                    url: '<?= PROOT ?>search/process',
                    type: "POST",
                    data: serializedData,
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function(results) {
                        $('#results').load(
                            '<?= PROOT ?>search/results', {
                                results
                            }
                        );
                        $('#footer').removeAttr('style', 'margin-top: 21.2rem !important;')
                    },
                    error: function() {
                        $('#results').load(
                            '<?= PROOT ?>restricted/error'
                        );
                    }
                });
            }
        });
    });
    var validator = $("#search_form").validate({
        errorLabelContainer: "#errorMessage div",
        wrapper: "div",
        rules: {
            // municipio: {
            //     required: true,
            // },
            cnpj: {
                number: true
            },
            cep: {
                number: true,
                maxlength: 8,
                minlength: 8
            }
        },
        messages: {
            // municipio: {
            //     required: "Município é obrigatório",
            // },
            cnpj: {
                number: 'CNPJ: Insira apenas números',
            },
            cep: {
                number: "Insira apenas Números",
                maxlength: "Insira o CEP corretamente",
                minlength: "Insira o CEP corretamente"
            }
        },
    });

    // function ApenasLetras(e, t) {
    //     try {
    //         if (window.event) {
    //             var charCode = window.event.keyCode;
    //         } else if (e) {
    //             var charCode = e.which;
    //         } else {
    //             return true;
    //         }
    //         if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123 || charCode == 32))
    //             return true;
    //         else
    //             return false;
    //     } catch (err) {
    //         alert(err.Description);
    //     }
    // }

    function ApenasNumeros(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            } else if (e) {
                var charCode = e.which;
            } else {
                return true;
            }
            if ((charCode > 47 && charCode < 58))
                return true;
            else
                return false;
        } catch (err) {
            alert(err.Description);
        }
    }
</script>
<?php $this->end(); ?>