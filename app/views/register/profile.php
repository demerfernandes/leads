<?php $this->setLayout('default'); ?>
<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Perfil'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div class="col-md-12  well" id="user-register">
    <h3 class=" ">Bem vindo ao seu perfil
        <?= $this->post['fname'] ?>
    </h3>
    <hr>
    <form class="form" id="formProfile" method="POST" novalidate>
        <div class="text-center pb-3">

            <img src="<?=PROOT?><?=currentUser()->image?>" class="rounded  border img-thumbnail" width="400px" height="400px" style="margin-top: 104px;margin-bottom: -265px;max-width:300px !important;max-height:200px !important;width: auto !important;height: auto !important;">
        </div>
        <div class="text-center pb-3">
            <button type="reset" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#imageChange">alterar imagem</button>
        </div>
        <div class="form-row col-12">
            <div class="form-inline font-weight-bold col-6">
                <label for="fname">Nome&nbsp;</label>
                <input type="text" id="fname" name="fname" class="form-control ml-5 col-6" value="<?= $this->post['fname'] ?>">
                <i class="fas fa-user-alt" style="margin-left: -1.7rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>
            </div>
            <div class="form-inline font-weight-bold col-6 justify-content-end">

                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" class="form-control ml-5 col-6" value="<?= $this->post['email'] ?>">
                <i class="fas fa-envelope" style="margin-left: -2rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>
            </div>
        </div>
        <div class="form-row col-12 pt-5">
            <div class="form-inline font-weight-bold col-6">
                <label for="lname">Sobrenome&nbsp;</label>
                <input type="text" id="lname" name="lname" class="form-control ml-1 col-6" value="<?= $this->post['lname'] ?>">&nbsp;&nbsp;
                <i class="fas fa-user-alt" style="margin-left: -2rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>

            </div>
            <div class="form-inline font-weight-bold col-6 justify-content-end">
                <label for="b_date">Aniversário</label>
                <input type="date" id="b_date" style="padding: .375rem 14.75rem .375rem .75rem;" name="b_date" class="form-control ml-5 col-6" value="<?= $this->post['b_date'] ?>">&nbsp;&nbsp;
                <i class="fas fa-birthday-cake" style="margin-left: -2.4rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>
            </div>
        </div>
        <div class="form-row col-12 pt-5">
            <div class="form-inline font-weight-bold col-6">
                <label for="phone">Telefone&nbsp;&nbsp;</label>
                <input type="tel" id="phone" name="phone" class="form-control ml-4 col-6" value="<?= $this->post['phone'] ?>">
                <i class="fas fa-phone" style="margin-left: -1.7rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>
            </div>
            <div class="form-inline font-weight-bold col-6 justify-content-end">
                <label for="position">Cargo&nbsp;</label>
                <input type="text" id="position" name="position" class="form-control ml-5 col-6" value="<?= $this->post['position'] ?>"> &nbsp;&nbsp;
                <i class="fas  fa-id-badge" style="margin-left: -2.2rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>

            </div>
        </div>
        <div class="form-row col-12 pt-5">
            <div class="form-inline font-weight-bold col-6">
                <label for="position">Time&nbsp;&nbsp;&nbsp;</label>
                <input type="text" disabled id="team" name="team" class="form-control ml-5 col-6" value="<?= $this->post['team'] ?>">
                <i class="fas fa-users" style="margin-left: -1.7rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>
            </div>
            <div class="form-inline font-weight-bold col-6 justify-content-end">
                <label for="position">Nível de acesso&nbsp;</label>
                <input type="text" disabled id="acl" name="acl" class="form-control ml-5 col-6" value="<?=  $this->post['acl'] ?>">
                <i class="fas fa-crown" style="margin-left: -2.2rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>

            </div>
        </div>
        <div class="form-row col-12 pt-5">
            <div class="form-inline font-weight-bold col-6">
                <label for="username " readonly>Username&nbsp;</label>
                <input type="text" disabled id="username" name="username" class="form-control ml-3 col-6" value="<?=$this->post['username'] ?>">
                <i class="fas fa-user-tag" style="margin-left: -1.7rem;z-index: 99;color:rgb(55, 165, 204) !important;"></i>
            </div>
        </div>
        <span class="text-right " id="errorMessage">
            <div class="display-errors ">
                <?= $this->displayErrors; ?>
            </div>

        <div class="row align-items-center justify-content-end mr-4 " id="pading-top">
            <input type="submit" class="btn btn-outline-primary btn-lg" value="Salvar">
        </div>
    </form>
</div>
<div class="modal  fade" tabindex="-1" id="imageChange" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nova foto de perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
</div>
<form method="post" enctype="multipart/form-data" action="profileImage">
    <div class="modal-body">
        <div class="custom-file">
            <div class="row">
                <div class="display-file-upload container col-md-8 pt-5"><img src="" class="" id="file-upload" alt="" width="500px"></div>
            </div>
            <div class="row">
                <input type="file" class="custom-file-input" id="profile-img" name="profile-img" accept="image/*">
                <label class="custom-file-label" for="profile-img" data-browse="Procurar">Selecione um arquivo</label>
            </div>
            <div id="upload-demo" class="croppie-container">
                <div class="cr-boundary" aria-dropeffect="none"><canvas class="cr-image" alt="preview" aria-grabbed="false"></canvas>
                    <div class="cr-viewport cr-vp-circle" style="width: 100px; height: 100px;" tabindex="0"></div>
                    <div class="cr-overlay"></div>
                </div>
                <div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001" aria-label="zoom"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-outline-primary">Salvar alterações</button>
    </div>
</form>
</div>
</div>
</div>

<script>
    $("#customFileLangHTML").change(function() {
        readURL(this);
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#file-upload').attr('src', e.target.result);
                $('#file-upload').addClass('img-thumbnail')
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#formProfile").validate({
        rules: {
            fname: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            lname: {
                required: true,
                minlength: 4,
                maxlength: 20
            },
            email: {
                required: true,
                email: true,
            },
            b_date: {
                date: true,
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 14
            },
            position: {
                required: true,
                minlength: 5,
                maxlength: 30
            },
        },
        messages: {
            fname: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
                maxlength: 'Nome muito Longa, insira corretamente'
            },
            lname: {
                required: 'Esse campo é obrigatório',
                minlength: 'Sobrenome muito curto, insira corretamente',
                maxlength: 'Sobrenome muito Longa, insira corretamente'
            },
            email: {
                required: 'Esse campo é obrigatório',
                email: 'insira um E-mail válido'
            },
            b_date: {
                date: 'Insira uma data válida',
                required: 'Esse campo é obrigatório'
            },
            phone: {
                required: 'Esse campo é obrigatório',
                minlength: 'Telefone muito curto, insira corretamente',
                maxlength: 'Telefone muito Longo, insira corretamente',
                number: 'Insira apenas números'
            },
            position: {
                required: 'Esse campo é obrigatório',
                minlength: 'Cargo muito curto, insira corretamente',
                maxlength: 'Cargo muito Longo, insira corretamente',
            },
        }

    });
</script>
<!-- <link rel="stylesheet" type="text/css" href="<?= PROOT ?>public/css/croppie.css" rel="stylesheet">
<script type="text/javascript" src="<?= PROOT ?>public/js/croppie.js"></script> -->

<?php $this->end(); ?>