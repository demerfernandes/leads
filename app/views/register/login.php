<?php $this->start('head'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div class="page" data-aos="fade-left" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
    <div class="container1">
        <div class="left">
            <div class="login">Login</div>
            <div class="eula">Bem vindo ao Sistema Leads</div>
            <div class="mx-auto text-center" style="font-size: 100px;"><i class="fa fa-user-circle"></i></div>

        </div>
        <div class="right row align-items-top">
            <form id="formLogin" class="container" action="<?= PROOT ?>register/login" method="post" novalidate>
                <div class="form-group col-md-auto" id="login-reponsivo">
                    <label id="message-email" for="username">Nome de Usuário</label>
                    <input type="text" name="username" id="username1" autofocus>
                    <label for="password" id="message-password">Senha</label>
                    <img onclick="show()" title="Mostrar a senha" id="olho-login" src="<?= PROOT ?>public/imgs/olho.png" alt="" width="30px">
                    <input type="password" id="password1" name="password">

                </div>
                <div class="form-group form-check">
                    <input type="checkbox" checked class="form-check-input" id="remember_me " name="remember_me ">
                    <label class="form-check-label" for="remember_me">Lembrar de mim</label>
                </div>
                <div class="form-group col-md-auto">
                    <button class="btn btn-outline-primary" value="Login " id="submit" type="submit">Entrar</button>
                </div>
                <div class="form-group col-md-auto">
                    <a href="<?= PROOT ?>register/register" class="text text-primary">Registrar-se</a>
                </div>

                <span class="text-right " id="errorMessage">
                    <div class="display-errors ">
                        <?= $this->displayErrors; ?>
                    </div>

        </div>
    </div>
</div>
<script>
    function show() {
        var senha = document.getElementById("password1");
        if (senha.type === "password") {
            senha.type = "text";
        } else {
            senha.type = "password";
        }
    }
</script>
<script>
    var form = $("#formLogin").validate({
        rules: {
            username: {
                required: true,
                minlength: 4,
                maxlength: 30
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
        },
        messages: {
            username: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome de usuário muito curto, insira corretamente',
                maxlength: 'Nome de usuário muito Longa, insira corretamente'
            },
            password: {
                required: 'Esse campo é obrigatório',
                minlength: 'Senha muito curta, insira corretamente',
                maxlength: 'Senha muito Longa, insira corretamente'
            },
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit();
        }
    });
</script>

<?php $this->end(); ?>