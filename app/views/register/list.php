<?php $this->setLayout('default'); ?>
<?php $this->setSiteTitle('Usuários'); ?>
<?php $this->start('body'); ?>

<h3 class="text-center empresas">Lista de Usuários</h3>
<style>
    #table-users_length {
        margin-bottom: -3rem;
        padding-left: 3rem;
    }
</style>

<div class="col-md-12" id="resultado">
    <div class="" id="results">
        <table class="table col-md-12 table-borderless" id="table-users">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">Nome de Usuário</th>
                    <th scope="col">Team</th>
                    <th scope="col">Id Empresa</th>
                    <th scope="col">Tipo de Usuário</th>
                    <th scope="col">Edição</th>
                </tr>
            </thead>
            <tbody>
            </tbody>


        </table>
    </div>
</div>

</div>
<!-- Modal -->
<div class="modal fade" id="editCompany" tabindex="-1" role="dialog" aria-labelledby="newLeadModal" aria-hidden="true">
    <div class="modal-xl modal-dialog" role="document">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#table-users').DataTable({
            // "scrollY": "800px",
            // "paging": false,
            "processing": true,
            //"serverSide": true,
            "ajax": {
                "url": 'results',
                "type": "POST",
            },
            "columns": [{
                "data": "fname"
            }, {
                "data": "lname"
            }, {
                "data": "username"
            }, {
                "data": "team"
            }, {
                "data": "company_id"
            }, {
                "data": "acl"
            }, {
                "defaultContent": "<button class='btn-leads btn-outline-primary btn' id='' data-toggle='modal' data-target='#editCompany'>ver</button>"
            }]

        });
        var table = $('#table-users').DataTable();
        var response = $('#table-users tbody').on('click', 'button', function() {
            blockUi('#table-users tbody');
            var data = table.row($(this).parents('tr')).data();
            var params = data.id
            $.ajax({
                url: '<?= PROOT ?>register/process',
                type: "POST",
                data: {
                    params
                },
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data) {
                    blockUi('.modal-content');
                    $('.modal-content').load(
                        '<?= PROOT ?>register/modal', {
                            data
                        }
                    );
                },
                error: function() {
                    alert('Something went wrong!');
                }
            });

        });

    });
</script>

<?php $this->end(); ?>