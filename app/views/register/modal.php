<?php $this->setLayout('no_layout'); ?>
<?php $user = $_POST['data']; ?>


<div class="container">
    <form id="edit-user" method="post" novalidate>
        <div class="modal-header">
            <h5 class="modal-title" id="newLeadModal"><b>Editar Usuário</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <input required hidden type="text" id="id" name="id" value="<?= $user['id'] ?>">
            <div class="form-row">
                <div class="form-group col-6 text-info">
                    <label for="fname">Nome</label>
                    <input type="text" id="fname" name="fname" class="form-control" value="<?= $user['fname'] ?>">
                </div>
                <div class="form-group col-6 text-info">
                    <label for="lname">Sobrenome</label>
                    <input type="text" id="lname" name="lname" class="form-control" value="<?= $user['lname'] ?>">
                </div>
                <div class="form-group col-8 text-info">
                    <label for="email">E-mail</label>
                    <input type="email" id="email" name="email" class="form-control" value="<?= $user['email'] ?>">
                </div>
                <div class="form-group col-4 text-info">
                    <label for="b_date">Aniversário</label>
                    <input type="date" id="b_date" name="b_date" class="form-control" value="<?= $user['b_date'] ?>">
                </div>
                <div class="form-group col-6 text-info">
                    <label for="phone">Telefone</label>
                    <input type="tel" id="phone" name="phone" class="form-control" value="<?= $user['phone'] ?>">
                </div>
                <div class="form-group col-6 text-info">
                    <label for="position">Cargo</label>
                    <input type="text" id="position" name="position" class="form-control" value="<?= $user['position'] ?>">
                </div>
                <div class="form-group col-5 text-info">
                    <label for="username " readonly>Nome de Usuário</label>
                    <input type="text" id="username" name="username" class="form-control" value="<?= $user['username'] ?>">
                </div>
                <div class="form-group col-4 text-info">
                    <label for="team">Equipe</label>
                    <select class="custom-select" id="team" name="team" value="<?= $user['team'] ?>">
                        <option value=" Contabilidade">Contabilidade</option>
                        <option value="Juridico">Jurídico</option>
                        <option value="Comercial">Comercial</option>
                    </select>
                </div>
                <div class="col-2 text-warning">
                    <label for="acl">Tipo de usuário</label>
                    <select class="custom-select" id="acl" name="acl" value="<?= $user['acl'] ?>">
                        <option value='["User"]'>Usuário</option>
                        <option value='["Master"]'>Master</option>
                        <option value='["Admin"]'>Admin</option>
                    </select>

                </div>
            </div>
        </div>
        <div class=" modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
            <input class="btn btn-default btn-success" type="submit" name="submit" value="Salvar">
        </div>
    </form>
</div>
<script>
    var validator = $("#edit-user").validate({
        rules: {
            fname: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            lname: {
                required: true,
                minlength: 4,
                maxlength: 20
            },
            email: {
                required: true,
                email: true,
            },
            b_date: {
                date: true,
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 14
            },
            position: {
                required: true,
                minlength: 5,
                maxlength: 30
            },
            username: {
                required: true,
                minlength: 5,
                maxlength: 15,
            },
            acl: {
                required: true,
                minlength: 1
            },
            team: {
                required: true,
                minlength: 1,
            },
            company_name: {
                required: true,
                minlength: 1,
            }
        },
        messages: {
            username: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome de usuario muito curta, insira corretamente',
                maxlength: 'Nome de usuario muito longa, insira corretamente'
            },
            company_name: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
            },
            team: {
                required: 'Esse campo é obrigatório',
            },
            acl: {
                required: 'Esse campo é obrigatório',
            },
            fname: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
                maxlength: 'Nome muito Longa, insira corretamente'
            },
            lname: {
                required: 'Esse campo é obrigatório',
                minlength: 'Sobrenome muito curto, insira corretamente',
                maxlength: 'Sobrenome muito Longa, insira corretamente'
            },
            email: {
                required: 'Esse campo é obrigatório',
                email: 'insira um E-mail válido'
            },
            b_date: {
                date: 'Insira uma data válida',
                required: 'Esse campo é obrigatório'
            },
            phone: {
                required: 'Esse campo é obrigatório',
                minlength: 'Telefone muito curto, insira corretamente',
                maxlength: 'Telefone muito Longo, insira corretamente',
                number: 'Insira apenas números'
            },
            position: {
                required: 'Esse campo é obrigatório',
                minlength: 'Cargo muito curto, insira corretamente',
                maxlength: 'Cargo muito Longo, insira corretamente',
            },
        }

    });
    $(document).ready(function() {
        $("#edit-user").submit(function(e) {
            e.preventDefault();
            var $form = $(this);
            var serializedData = $form.serialize();
            if (validator.errorList.length >= 1) {
                return false;
            } else {
                blockUi('#edit-user');
                $.ajax({
                    url: '<?= PROOT ?>register/update',
                    type: 'POST',
                    data: serializedData,
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    success: function(response) {
                        $('#table-users tbody').unblock()
                        $("[data-dismiss=modal]").trigger({
                            type: "click"
                        });

                    },
                    error: function() {
                        alert("error");
                    }
                });
            }
        });
        $("#edit-user").on("submit", function() {
            $('#table-users').DataTable().ajax.reload();
        });
        $('.modal').on('hidden.bs.modal', function() {
            $('#table-users tbody').unblock()
        })

    });
</script>