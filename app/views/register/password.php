<?php $this->setLayout('default'); ?>
<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Trocar a Senha'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div class="col-md-6 offset-md-3 well" id="user-register">
	<h3 class="text-center">Trocar a Senha</h3>
	<hr>
	<form class="form" method="POST" id="user-pass">
		<div class="display-errors"><?= $this->displayErrors; ?></div>
		<div class="form-group">
			<label for="password">Nova Senha</label>
			<input type="password" id="password" name="password" class="form-control">
		</div>
		<div class="form-group">
			<label for="confirm">Confirme a Senha!</label>
			<input type="password" id="confirm" name="confirm" class="form-control">
		</div>
		<div class="text-center ">
			<input type="submit" class="btn btn-outline-primary btn-large" value="Salvar">
		</div>
	</form>
</div>

<script>
	$("#user-pass").validate({
		rules: {
			password: {
				required: true,
				minlength: 6,
				maxlength: 20
			},
			confirm: {
				equalTo: password
			}
		},
		messages: {
			password: {
				required: 'Esse campo é obrigatório',
				minlength: 'Senha muito curta, insira corretamente',
				maxlength: 'Senha muito Longo, insira corretamente'
			},
			confirm: {
				equalTo: 'insira as senhas iguais'
			}
		}
	});
</script>
<?php $this->end(); ?>