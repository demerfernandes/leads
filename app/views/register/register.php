<?php $this->setLayout('default'); ?>
<?php $this->start('head'); ?>
<?php $this->setSiteTitle('Novo Usuário'); ?>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
<div class="col-md-8 offset-md-2 well" id="user-register" data-aos="fade-down" data-aos-duration="500">
    <h3 class="text-center">Cadastro de Novo Usuário!</h3>
    <hr>
    <form id="register" class="form" method="POST">
        <div class="display-errors">
            <?= $this->displayErrors; ?>
        </div>
        <div class="form-row">
            <div class="form-group col-6">
                <label for="fname">Nome</label>
                <input type="text" id="fname" name="fname" class="form-control" value="<?= $this->post['fname'] ?>">
            </div>
            <div class="form-group col-6">
                <label for="lname">Sobrenome</label>
                <input type="text" id="lname" name="lname" class="form-control" value="<?= $this->post['lname'] ?>">
            </div>
            <div class="form-group col-8">
                <label for="email">E-mail</label>
                <input type="email" id="email" name="email" class="form-control" value="<?= $this->post['email'] ?>">
            </div>
            <div class="form-group col-4">
                <label for="b_date">Aniversário</label>
                <input type="date" min="1910-01-01" max="2020-01-01" id="b_date" title="insira uma data válida" name="b_date" class="form-control" value="<?= $this->post['b_date'] ?>">
            </div>
            <div class="form-group col-6">
                <label for="phone">Telefone</label>
                <input type="tel" id="phone" name="phone" class="form-control" value="<?= $this->post['phone'] ?>">
            </div>
            <div class="form-group col-6">
                <label for="position">Cargo</label>
                <input type="text" id="position" name="position" class="form-control" value="<?= $this->post['position'] ?>">
            </div>
            <div class="form-group col-5">
                <label for="username">Nome de Usuário</label>
                <input type="text" id="username" name="username" class="form-control" value="<?= $this->post['username'] ?>">
            </div>
            <div class="form-group col-7">
                <label for="address">Endereço</label>
                <input type="text" id="address" name="address" class="form-control" value="<?= $this->post['address'] ?>">
            </div>
            <div class="form-group col-6">
                <label for="password">Senha</label>
                <input type="password" id="password" name="password" class="form-control" value="">
            </div>
            <div class="form-group col-6">
                <label for="confirm">Confirme a Senha</label>
                <input type="password" id="confirm" name="confirm" class="form-control" value="">
            </div>
            <div class="form-group col-4">
                <label for="acl">Tipo de usuário</label>
                <select class="custom-select" id="acl" name="acl">
                    <option value="User">Usuário</option>
                    <option value="Master">Master</option>
                    <option value="Admin">Admin</option>
                </select>
            </div>
            <div class="form-group col-4">
                <label for="team">Equipe</label>
                <select class="custom-select" id="team" name="team">
                    <option value="Contabilidade">Contabilidade</option>
                    <option value="Juridico">Jurídico</option>
                    <option value="Comercial">Comercial</option>
                </select>
            </div>
            <div class="form-group col-4">
                <label for="company_name">Nome da Empresa</label>
                <input type="text" id="company_name" class="form-control" name="company_name" value="<?= $this->post['company_name'] ?>" autocomplete="off">
                <div id="suggesstion-box">
                    <ul id="company-list" class="list-group"></ul>
                </div>

            </div>
            <div class="form-group col-2" hidden>
                <label for="company_id">Id empresa</label>
                <input type="text" hidden id="company_id" name="company_id" class="form-control" value="<?= $this->post['company_id'] ?>">
            </div>
            <div class="form-group col-12">
                <div class="col-12 text-center mx-auto" id="message"></div>
            </div>
            <div class="pull-right">
                <input id="submit-user" type="submit" class="btn btn-outline-primary btn-large" value="Cadastrar">
            </div>
        </div>
    </form>
</div>

<script>
    function autoComplete() {
        $("#company_name").keyup(function() {
            $.ajax({
                type: "POST",
                url: "<?= PROOT ?>dashboard/autoCompleteCompany",
                data: 'keyword=' + $(this).val(),
                success: function(data) {
                    $("#company-list").empty();
                    $("#suggesstion-box").css("background", "#FFF");
                    $("#company-list").append('<span class="dropdown text-center text-success"">Selecione</span>');
                    for (var i = 0; i < data.length; i++) {
                        var obj = data[i];
                        $("#company-list").append('<li class="dropdown-item" id="' + obj.id + '">' + obj.razao_social + '</li>');
                        $("#suggesstion-box").show();
                    }
                    $click = $('#company-list li').click(function() {
                        selectCompany($(this).text());
                        setCompanyId($(this).attr("id"));
                    });

                }
            });
        });
    }

    //To select company name
    function selectCompany(val) {
        $("#company_name").val(val);
        $("#suggesstion-box").hide();

    }
    $(document).on("click", "#company_name", function(e) {
        $(this).val('');
        $("#company_id").val('');
        autoComplete();
    });

    function setCompanyId(name) {
        $('#company_id').val(name);
    }
    $("#register").validate({
        rules: {
            fname: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            lname: {
                required: true,
                minlength: 4,
                maxlength: 20
            },
            email: {
                required: true,
                email: true,
            },
            b_date: {
                date: true,
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 14
            },
            position: {
                required: true,
                minlength: 5,
                maxlength: 30
            },
            username: {
                required: true,
                minlength: 5,
                maxlength: 15,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 15
            },
            confirm: {
                equalTo: password,
                required: true
            },
            acl: {
                required: true,
                minlength: 1
            },
            team: {
                required: true,
                minlength: 1,
            },
            company_name: {
                required: true,
                minlength: 1,
            }
        },
        messages: {
            username: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome de usuario muito curta, insira corretamente',
                maxlength: 'Nome de usuario muito longa, insira corretamente'
            },
            company_name: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
            },
            team: {
                required: 'Esse campo é obrigatório',
            },
            acl: {
                required: 'Esse campo é obrigatório',
            },
            password: {
                required: 'Esse campo é obrigatório',
                minlength: 'Senha muito curta, insira corretamente',
                maxlength: 'Senha muito Longo, insira corretamente'
            },
            confirm: {
                equalTo: 'insira as senhas iguais',
                required: 'Esse campo é obrigatório',
            },
            fname: {
                required: 'Esse campo é obrigatório',
                minlength: 'Nome muito curto, insira corretamente',
                maxlength: 'Nome muito Longa, insira corretamente'
            },
            lname: {
                required: 'Esse campo é obrigatório',
                minlength: 'Sobrenome muito curto, insira corretamente',
                maxlength: 'Sobrenome muito Longa, insira corretamente'
            },
            email: {
                required: 'Esse campo é obrigatório',
                email: 'insira um E-mail válido'
            },
            b_date: {
                date: 'Insira uma data válida',
                required: 'Esse campo é obrigatório'
            },
            phone: {
                required: 'Esse campo é obrigatório',
                minlength: 'Telefone muito curto, insira corretamente',
                maxlength: 'Telefone muito Longo, insira corretamente',
                number: 'Insira apenas números'
            },
            position: {
                required: 'Esse campo é obrigatório',
                minlength: 'Cargo muito curto, insira corretamente',
                maxlength: 'Cargo muito Longo, insira corretamente',
            },
        }

    });
</script>

<?php $this->end(); ?>