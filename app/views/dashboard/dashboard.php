<div id="results">

</div>

<script>
    $(document).ready(function() {
        $.ajax({
                url: '<?= PROOT ?>search/process',
                type: "POST",
                data: serializedData,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(results) {
                    $('#results').load(
                        '<?= PROOT ?>dashboard/dashboard', {
                            results
                        }
                    );
                    $('#footer').removeAttr('style', 'margin-top: 21rem !important;')
                },
                error: function() {
                    $('#results').load(
                        '<?= PROOT ?>restricted/error'
                    );
                }
            });
    });

</script>