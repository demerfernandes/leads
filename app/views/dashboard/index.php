<?php $this->setLayout('default'); ?>
<?php $this->setSiteTitle('Dashboard'); ?>
<?php $this->start('body'); ?>

<style>
    .card-title {
        margin-bottom: .75rem;
        margin-top: .75rem;
    }

    #config {
        padding: 1rem;
        text-align-last: end;
    }
</style>
<div class="container text-lg-center mb-5">
    <h1 class="h1">Dashboard</h1>
</div>
<div class="container-fluid">
    <div class="card-deck">
        <div id='card-1' class="card bg-light ">
            <div class="card-header alert-warning" style="padding: 0.2rem 1rem;">
                <div class="row">
                    <h5 class="card-title col-9"></h5>
                    <i id="config" class="fas fa-cog col-3"></i>
                </div>
            </div>
            <div class="card-body text-center">
                <p class="card-text" style="font-size: 60px; cursor:pointer;"></p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <p class="card-text col-9" style="margin-bottom: 0;">
                        <small class="text-muted">
                        </small>
                    </p>
                    <button id="show" class="btn btn-sm btn-outline-warning col-2 mr-2">ver</button>
                </div>
            </div>
        </div>
        <div id='card-2' class="card bg-light ">
            <div class="card-header alert-danger" style="padding: 0.2rem 1rem;">
                <div class="row">
                    <h5 class="card-title col-9"></h5>
                    <i id="config" class="fas fa-cog col-3"></i>
                </div>
            </div>
            <div class="card-body text-center">
                <p class="card-text" style="font-size: 60px; cursor:pointer;"></p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <p class="card-text col-9" style="margin-bottom: 0;">
                        <small class="text-muted">
                        </small>
                    </p>
                    <button id="show" class="btn btn-sm btn-outline-danger col-2 mr-2">ver</button>
                </div>
            </div>
        </div>

        <div id='card-3' class="card bg-light ">
            <div class="card-header alert-success" style="padding: 0.2rem 1rem;">
                <div class="row">
                    <h5 class="card-title col-9"></h5>
                    <i id="config" class="fas fa-cog col-3"></i>
                </div>
            </div>
            <div class="card-body text-center">
                <p class="card-text" style="font-size: 60px; cursor:pointer;"></p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <p class="card-text col-9" style="margin-bottom: 0;">
                        <small class="text-muted">
                        </small>
                    </p>
                    <button id="show" class="btn btn-sm btn-outline-success col-2 mr-2">ver</button>
                </div>
            </div>
        </div>

        <div id='card-4' class="card bg-light ">
            <div class="card-header alert-info" style="padding: 0.2rem 1rem;">
                <div class="row">
                    <h5 class="card-title col-9"></h5>
                    <i id="config" class="fas fa-cog col-3"></i>
                </div>
            </div>
            <div class="card-body text-center">
                <p class="card-text" style="font-size: 60px; cursor:pointer;"></p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <p class="card-text col-9" style="margin-bottom: 0;">
                        <small class="text-muted">
                        </small>
                    </p>
                    <button id="show" class="btn btn-sm btn-outline-info col-2 mr-2">ver</button>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="showTable">
    <div class="col-md-12 mt-5" id="resultado" style="display: none;">
        <table class="table col-md-12 table-borderless " id="leads-table" data-order='[[ 0, "asc" ]]' data-page-length='10'>
            <thead class="thead-dark">
                <tr>
                    <th id="creator" scope="col">Responsável</th>
                    <th id="contactName" scope="col">Nome do Contato</th>
                    <th scope="col">Estado</th>
                    <th id="lastContact" scope="col">Último contato</th>
                    <th id="date" scope="col">Data</th>
                    <th scope="col">Conversão</th>
                    <th scope="col">Edição</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</section>
<div class="modal fade" id="editLead" tabindex="-1" role="dialog" aria-labelledby="newLeadModal" aria-hidden="true">
    <div class="modal-xl modal-dialog" role="document">
        <div class="modal-content" id="modal-content">
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var dataForTable;
        var titlePDF = [];
        var table = $("#leads-table").DataTable({
            "searching": false,
            "processing": true,
            dom: 'Bfrtip',
            buttons: [{
                text: 'Gerar PDF',
                filename: 'titulo',
                extend: 'pdfHtml5',
                download: 'open',
                title: 'Impressão Dashboard',
                className: 'btn btn-outline-primary mb-3',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            }],
            "columns": [{
                "data": "user_name"
            }, {
                "data": "contact_name"
            }, {
                "data": "status"
            }, {
                "data": "last_contact_time"
            }, {
                "data": "last_contact_status"
            }, {
                "data": "conversion"
            }, {
                "defaultContent": "<button class='btn-leads btn-outline-primary btn' id='++' data-toggle='modal' data-target='#editLead'>ver</button>"
            }]
        });

        var jsonCard1 = {
            'type': 'card',
            'last_contact_status': '',
            'status': 'em_andamento',
            'period': '30 dias',
            'by_user_id': ''
        };

        var jsonCard2 = {
            'type': 'card',
            'last_contact_status': '',
            'status': 'nao',
            'period': '30 dias',
            'by_user_id': ''
        };

        var jsonCard3 = {
            'type': 'card',
            'last_contact_status': '',
            'status': 'sim',
            'period': '30 dias',
            'by_user_id': ''
        };

        var jsonCard4 = {
            'type': 'card',
            'last_contact_status': '',
            'status': '',
            'period': '30 dias',
            'by_user_id': ''
        };

        function updateAjax(jsonData) {
            var dataToReturn;
            $('#leads-table tbody').unblock()
            $.ajax({
                async: false,
                url: '<?= PROOT ?>dashboard/processDisplay',
                type: "POST",
                data: jsonData,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(response) {
                    $('#footer').attr('style', 'margin-top: 12.4rem !important;');
                    dataToReturn = response;
                }
            });
            return dataToReturn;
        }

        function updateCards() {
            $('#card-1').html(function() {
                var that = this;
                jsonCard1.type = 'card';
                $(that).find("p.card-text").text(updateAjax(jsonCard1));
                $(that).find(".card-title").text('Em andamento');
                $(that).find(".card-footer p ").text('Últimos 30 dias');
            });

            $('#card-2').val(function() {
                var that = this;
                jsonCard2.type = 'card';
                $(that).find("p.card-text").text(updateAjax(jsonCard2));
                $(that).find(".card-title").text('Recusados');
                $(that).find(".card-footer p").text('Últimos 30 dias');
            });

            $('#card-3').val(function() {
                var that = this;
                jsonCard3.type = 'card';
                $(that).find("p.card-text").text(updateAjax(jsonCard3));
                $(that).find(".card-title").text('Contratos feitos');
                $(that).find(".card-footer p").text('Últimos 30 dias');
            });

            $('#card-4').val(function() {
                var that = this;
                jsonCard4.type = 'card';
                $(that).find(".card-title").text('Todos os Leads');
                $(that).find("p.card-text").text(updateAjax(jsonCard4));
                $(that).find(".card-footer p").text('Últimos 30 dias');
            });
        }

        function updateTable(data) {
            $('#footer').removeAttr('style', 'margin-top: 12.4rem !important;')
            $('#resultado').fadeIn(800);
            table.clear().rows.add(data).draw();
            $('#showTable').unblock();

        }

        $('#card-1 #show').on('click', function() {
            blockUi('#showTable')
            jsonCard1.type = 'chart';
            dataForTable = updateAjax(jsonCard1);
            updateTable(dataForTable);
        });

        $('#card-2 #show').on('click', function() {
            blockUi('#showTable')
            jsonCard2.type = 'chart';
            dataForTable = updateAjax(jsonCard2);
            updateTable(dataForTable);
        });

        $('#card-3 #show').on('click', function() {
            blockUi('#showTable')
            jsonCard3.type = 'chart';
            dataForTable = updateAjax(jsonCard3);
            updateTable(dataForTable);
        });

        $('#card-4 #show').on('click', function() {
            blockUi('#showTable')
            jsonCard4.type = 'chart';
            dataForTable = updateAjax(jsonCard4);
            updateTable(dataForTable);
        });

        //button on modal

        $('#leads-table tbody').on('click', 'button', function() {
            var data = table.row($(this).parents('tr')).data();
            var params = "lead-" + data.id
            blockUi('#leads-table tbody');
            $.ajax({
                url: '<?= PROOT ?>leads/register',
                type: "POST",
                data: {
                    params
                },
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function(data) {
                    blockUi('.modal-content');
                    $('.modal-content').load(
                        '<?= PROOT ?>leads/modal', {
                            data
                        }
                    );
                },
                error: function() {
                    alert('Something went wrong!');
                }
            });

        });

        updateCards();

        $("#editLead").on("hidden.bs.modal", function() {
            updateCards();
        });
        $('.modal').on('hidden.bs.modal', function() {
            $('#leads-table tbody').unblock()
        })

    });
</script>


<script type="text/javascript" src="<?= PROOT ?>public/js/dtButtons.min.js"></script>
<script type="text/javascript" src="<?= PROOT ?>public/js/pdfMake.min.js"></script>
<script src="<?= PROOT ?>public/js/pdfFont.js"></script>
<script type="text/javascript" src="<?= PROOT ?>public/js/buttonsHtml.min.js"></script>

<?php $this->end(); ?>