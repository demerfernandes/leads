<?php $this->setLayout('default'); ?>
<?php $this->setSiteTitle('Dashboard'); ?>
<?php $this->start('body'); ?>

<div class="row">
	<div class="frmSearch">
		<input type="text" id="search-box" placeholder="Company Name" />
		<input type="text" hidden id="company_id" value="" />
		<div id="suggesstion-box">
			<ul id="company-list" class="list-group"></ul>
		</div>
	</div>
</div>


<script>
	// AJAX call for autocomplete 
	$(document).ready(function() {
		$("#search-box").keyup(function() {
			$.ajax({
				type: "POST",
				url: "<?= PROOT ?>dashboard/autoCompleteCompany",
				data: 'keyword=' + $(this).val(),
				beforeSend: function() {
					$("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
				},
				success: function(data) {
					$("#company-list").empty();
					$("#suggesstion-box").css("background", "#FFF");
					for (var i = 0; i < data.length; i++) {
						var obj = data[i];
						$("#company-list").append('<li class="list-group-item">' + obj.name + '</li>');
					}

					$('#company-list li').click(function() {
						selectCompany($(this).text());
						setCompanyId(data, $(this).text());
					});
				}
			});
		});
	});
	//To select company name
	function selectCompany(val) {
		$("#search-box").val(val);
		$("#suggesstion-box").hide();
	}

	function setCompanyId(data, name) {
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			if (obj.name == name) {
				$('#company_id').val(obj.id);
			}
		}
	}
</script>

<?php $this->end(); ?>