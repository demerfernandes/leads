<?php 
  $menu = Router::getMenu('menu_acl');
  $currentPage = currentPage();
?>
<nav class="fixed-top navbar navbar-expand-lg navbar-light bg-light" id="nav-menu" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="500">
    <a class="navbar-brand" href="<?=PROOT?>dashboard/index"><img src="<?=PROOT?>public/imgs/logo.png" width="150px" alt=""></a>
    <ul class="navbar-nav navbar" id="user-profile-fix">
        <?php if(currentUser()): ?>
        <li class="nav-item">

            <i style="font-size:40px; color:white; border-radius: 20px;box-shadow: 0px 0px 3px 1px #3c3c3c96;">
             
            </i>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Olá <?=currentUser()->username?>
          </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item rounded" href="<?=PROOT?>register/profile">Perfil</a>
                <a class="dropdown-item rounded" href="<?=PROOT?>register/password">Trocar Senha</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=PROOT?>register/logout">Sair</a>
            </div>
        </li>
        <?php else: ?>
        <li class="nav-item <?=$active?>"><a class="nav-link" href="<?php echo PROOT.'register/login' ?>">Entrar</a></li>
        <?php endif; ?>
    </ul>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dashboard-menu" aria-controls="main_menu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>




    <div class="collapse navbar-collapse" id="dashboard-menu">
        <ul class="navbar-nav mr-auto">
            <?php foreach($menu as $key => $val): 
        $active = ''; ?>
            <?php if(is_array($val)): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?=$key?>
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php foreach($val as $k => $v): 
                $active = ($v == $currentPage)? 'active':''; ?>
                    <?php if($k == 'separator'): ?>
                    <div class="dropdown-divider"></div>
                    <?php else: ?>
                    <a class="dropdown-item <?=$active?>" href="<?=$v?>">
                        <?=$k?>
                    </a>
                    <?php endif; ?>

                    <?php endforeach; ?>
                </div>
            </li>
            <?php else: 
          $active = ($val == $currentPage)? 'active':'';?>
            <li class="nav-item <?=$active?>">
                <a class="nav-link" href="<?=$val?>">
                    <?=$key?>
                </a>
            </li>
            <?php endif; ?>
            <?php endforeach; ?>

        </ul>
        <ul class="navbar-nav navbar-right" id="user-profile">
            <?php if(currentUser()): ?>
            <li class="nav-item">
                <!-- <i style="font-size:40px; color:white; border-radius:  20px;box-shadow: 0px 0px 3px 1px #3c3c3c96;" class="fa fa-user-circle" aria-hidden="true"></i> -->
                <div class="border" style="margin-left: -70px; max-width: 70px; position: absolute; background-size: cover;background-position: center center;width: 4%;height: 70%;z-index: -1; background-image: url('<?=PROOT?><?=currentUser()->image?>');" width="50px">
                </div>
            </li>
            <li class="nav-item dropdown pt-1">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Olá <?=currentUser()->username?>
          </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item rounded" href="<?=PROOT?>register/profile">Perfil</a>
                    <a class="dropdown-item rounded" href="<?=PROOT?>register/password">Trocar Senha</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?=PROOT?>register/logout">Sair</a>
                </div>
            </li>
            <?php else: ?>
            <li class="nav-item <?=$active?>"><a class="nav-link" href="<?php echo PROOT.'register/login' ?>">Entrar</a></li>
            <?php endif; ?>
        </ul>

    </div>
</nav>