<section class="m-xl-auto mb-auto mt-auto" id="footer" id="footer">
    <div class="container footer-center">
        <div class="row">
            <div class="col">
                <footer>
                    <div class="row">
                        <div class="col-sm-6 col-md-4 footer-navigation">
                            <h3>
                                <a href="/">
                                    <img src="<?= PROOT ?>public/imgs/logo.png" width="130px">
                                </a>
                            </h3>
                            <p class="links">
                                <a href="<?= PROOT ?>dashboard">Home</a>
                                <strong> · </strong>
                                <a href="<?= PROOT ?>dashboard">Dashboard</a>
                                <strong> · </strong>
                                <a href="">Quem somos</a>
                                <strong> · </strong>
                                <a href="<?= PROOT ?>register/login">Login</a>
                                <strong> · </strong>
                            </p>
                            <div class="company-name">
                                Project LEADS ©
                                <a id="copyright"></a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 footer-contacts">
                            <div>
                                <i class="fas fa-map-marked-alt footer-contacts-icon"></i>
                                <p>
                                    <a id="maps" href="https://g.page/AcruxTecnologias?share">
                                        <span class="new-line-span">Av. Cel. Procópio Gomes, 419</span> Bucarein, Joinville - SC
                                    </a>
                                </p>
                            </div>
                            <div>
                                <i class="fa fa-phone footer-contacts-icon"></i>
                                <a href="tel:(47) 3801-1970" target="blank">
                                    <p class="footer-center-info email text-left">(47) 3801-1970</p>
                                </a>
                            </div>
                            <div>
                                <i class="fab fa-whatsapp footer-contacts-icon"></i>
                                <a href="https://api.whatsapp.com/send?phone=5547991281110&text=Ol%C3%A1.%20Gostaria%20de%20saber%20mais%20sobre%20os%20servi%C3%A7os%20da%20Acrux%20Tecnologias" target="blank">
                                    <p class="footer-center-info email text-left">(47) 99128-1110</p>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 footer-about">
                            <h4>Sobre a Empresa</h4>
                            <p> Acompanhe nossas redes socias</p>
                            <div class="social-links social-icons">
                                <a href="https://www.facebook.com/acruxtecnologias" target="blank">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                                <a href="https://api.whatsapp.com/send?phone=5547991281110&text=Ol%C3%A1.%20Gostaria%20de%20saber%20mais%20sobre%20os%20servi%C3%A7os%20da%20Acrux%20Tecnologias" target="blank">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/acruxtecnologias" target="blank">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                                <a href="https://www.instagram.com/acruxtecnologias/" target="blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                            <br>
                            <div>
                                Desenvolvimento
                                <a href="https://acruxtecnologias.com.br/" target="_blank"> Acrux Tecnologias</a>
                            </div>
                        </div>
                    </div>

            </div>
            </footer>
        </div>
    </div>
    </div>
</section>
<script async defer>
    document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
</script>