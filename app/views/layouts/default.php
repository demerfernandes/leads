<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="">
    <link rel="shortcut icon" href="<?= PROOT ?>public/imgs/ico.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?= PROOT ?>public/css/bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?= PROOT ?>public/css/styles.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?= PROOT ?>public/css/datatables.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?= PROOT ?>public/fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= PROOT ?>public/css/aos.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?= PROOT ?>public/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/aos.min.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/jquerytables.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/chart.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/popper.min.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/datatables.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/validate.js"></script>
    <script type="text/javascript" src="<?= PROOT ?>public/js/bs-custom-file-input.js"></script>
    
    <script type="text/javascript" src="<?= PROOT ?>public/js/blockUi.js"></script>
    <noscript>
        <META HTTP-EQUIV="Refresh" CONTENT="0; URL=<?= PROOT ?>restricted/error">
    </noscript>


    <?= $this->content('head'); ?>
    <title>
        <?= $this->siteTitle(); ?>
    </title>
</head>

<body>
    <style>
        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 999;
            background-color: white;
        }

        #loader {
            display: block;
            position: absolute;
            left: 50%;
            top: 50%;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: rgb(55, 165, 204) !important;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        #loader:before {
            content: "";
            position: absolute;
            top: 5px;
            left: 5px;
            right: 5px;
            bottom: 5px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: rgb(55, 165, 204) !important;
            -webkit-animation: spin 3s linear infinite;
            animation: spin 3s linear infinite;
        }

        .c-loader {
            animation: is-rotating 1s infinite;
            border: 6px solid #e5e5e5;
            border-radius: 50%;
            border-top-color: rgb(55, 165, 204);
            height: 50px;
            margin-top: auto;
            margin-bottom: auto;
            width: 50px;
        }

        @keyframes is-rotating {
            to {
                transform: rotate(1turn);
            }
        }

        #loader:after {
            content: "";
            position: absolute;
            top: 15px;
            left: 15px;
            right: 15px;
            bottom: 15px;
            border-radius: 50%;
            border: 3px solid transparent;
            border-top-color: rgb(55, 165, 204) !important;
            -webkit-animation: spin 1.5s linear infinite;
            animation: spin 1.5s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
    <?php include 'main_menu.php' ?>
    <div class="container-fluid col-md-10 section" id="default" style="min-height: cal(100% - 125px);">
        <?= $this->content('body'); ?>
    </div>
    <?php include 'main_footer.php' ?>
    <script async defer>
        AOS.init();
        $(document).ready(function () {
            bsCustomFileInput.init()
        })
    </script>

    <script>
        $(window).on('load', function() {
            $('#preloader .inner').fadeOut();
            $('#preloader').delay(500).fadeOut('slow');
            $('body').delay(500).css({
                'overflow': 'visible'
            });

        })
    </script>
    <div id="preloader">
        <div id="loader">
        </div>
    </div>
</body>

<script>
    function blockUi(block) {
        $(block).block({
            message: '<div class="c-loader ml-auto mr-auto"></div>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    }
</script>

</html>