<?php

class Companies extends Model
{
	public function __construct($company = '')
	{
		$table = 'companies';
		parent::__construct($table);
		$this->_softDelete = true;
	}

	public function registerNewCompany($params)
	{
		$this->assign($params);
		$this->deleted = 0;
		$this->save();
		$this->_createLeadsTable($this->lastInsertId());
	}

	public function updateCompany($params)
	{
		$this->assign($params);
		$this->deleted = 0;
		$this->last_update = date("Y-m-d H:i:s");
		$this->save();
	}

	public function findById($id)
	{
		return $this->findFirst(['conditions' => "id = ?", 'bind' => [$id]]);
	}

	public function findByName($name)
	{
		return $this->find(['conditions' => "razao_social LIKE ?", 'bind' => ['%' . $name . '%']]);
	}

	public function findAll()
	{
		return $this->find();
	}
	protected function _createLeadsTable($company_id)
	{
		$sql = "CREATE TABLE IF NOT EXISTS leads_{$company_id} (
					id INT NOT NULL AUTO_INCREMENT,  
					user_id INT NOT NULL,
					created_by VARCHAR(250) NULL, 
					user_name VARCHAR(250) NULL, 
					company_id INT NOT NULL, 
					consulta_id INT NOT NULL,
					contact_name VARCHAR(250) NULL, 
					contact_role VARCHAR(250) NULL, 
					contact_phone VARCHAR(150) NULL, 
					contact_email VARCHAR(250) NULL,  
					last_contact_status VARCHAR(250) NULL,  
					last_contact_time VARCHAR(150) NULL,  
					status VARCHAR(150) NULL, 
					notes TEXT NULL, 
					conversion VARCHAR(10) NULL,
					created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
					last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					deleted TINYINT NOT NULL DEFAULT '0',
					PRIMARY KEY (id)
				) ENGINE = InnoDB;";
		if (!$this->_db->query($sql)->error()) {
			return true;
		}
		return false;
	}
}
