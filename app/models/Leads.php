<?php

class Leads extends Model
{
	public function __construct($lead = '')
	{
		$table = 'leads_' . currentUser()->company_id;
		$this->setTable($table);
		parent::__construct($table);
		$this->_softDelete = true;
	}

	public function insertLead($table, $params)
	{

		$find = $this->_db->findFirst($table, ['conditions' => 'consulta_id = ?', 'bind' => [$params['consulta_id']]]);
		if ($find) return $find;
		return $this->insert($params);
	}

	public function findCompanyLeads($table, $params)
	{
		return $this->_db->find($table, $params);
	}

	public function showCompanyLeads($params)
	{
		return $this->_db->findInnerJoin($params);
	}

	public function showUserLeads($params)
	{
		$filterUser =  't2.user_id = ' . currentUser()->id;
		array_push($params['conditions'], $filterUser);
		return $this->_db->findInnerJoin($params);
	}

	public function updateLead($params)
	{
		$this->assign($params);
		$this->last_update = $this->getCurrentDate();
		$this->deleted = 0;
		$this->save();
	}

	public function lastInsert()
	{
		return $this->lastInsertId();
	}
}
