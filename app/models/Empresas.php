<?php

class Empresas extends Model
{
	public function __construct($empresa = '')
	{
		$table = 'empresas';
		parent::__construct($table);
		$this->_softDelete = true;
	}

	public function findByCnpj($cnpj)
	{
		return $this->findFirst(['conditions' => "cnpj = ?", 'bind' => [$cnpj]]);
	}

	public function findByParams($valid = [], $leadsTable)
	{
		$conditions = [];
		$conditionString = '';
		$bind = [];
		foreach ($valid as $key => $val) {
			$conditions[] = $key . ' LIKE ?';
			$conditionString .= $key . ' LIKE ? AND ';
			$bind[] = '%' . $val . '%';
		}

		$conditionString .= ' situacao <> ? AND';
		$conditionString .= ' deleted != 1';
		array_push($bind, '08');

		$sql = "SELECT * FROM empresas WHERE " . $conditionString . " AND NOT EXISTS ( SELECT * FROM " . $leadsTable . " WHERE " . $leadsTable . ".consulta_id=empresas.id) ORDER BY data_inicio_ativ DESC LIMIT 100";
		if ($this->_db->query($sql, $bind)) {
			return $this->_db->results();
		}
		return false;
	}
}
