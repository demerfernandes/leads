<?php

class Consultas extends Model
{
	public function __construct($consulta = '')
	{
		$table = 'consultas';
		parent::__construct($table);
		$this->_softDelete = true;
	}

	public function findByProtocolo($protocolo)
	{
		return $this->findFirst(['conditions' => "protocolo = ?", 'bind' => [$protocolo]]);
	}

	public function findByParams($valid = [], $leadsTable)
	{
		$conditions = [];
		$conditionString = '';
		$bind = [];
		foreach ($valid as $key => $val) {
			$conditions[] = $key . ' LIKE ?';
			$conditionString .= $key . ' LIKE ? AND ';
			$bind[] = '%' . $val . '%';
		}
		$conditionString .= ' deleted != 1';

		$sql = "SELECT * FROM consultas WHERE " . $conditionString . " AND NOT EXISTS ( SELECT * FROM " . $leadsTable . " WHERE " . $leadsTable . ".consulta_id=consultas.id) ORDER BY date DESC LIMIT 100";

		if ($this->_db->query($sql, $bind)) {
			return $this->_db->results();
		}
		return false;
	}
}
